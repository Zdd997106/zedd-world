FROM node:16.13.0-stretch-slim AS setter
WORKDIR /app
COPY . .
COPY ./server/.env.sample ./server/.env.local

RUN npm install
RUN npm run build:server

FROM node:16.13.0-stretch-slim AS runner
WORKDIR /app
COPY . .
COPY --from=setter /app/server/dist/index.js ./server.js
COPY --from=setter /app/node_modules ./node_modules
COPY .env.sample .env.local

RUN npm run build:client

EXPOSE 3000

CMD ["node", "server.js"]
