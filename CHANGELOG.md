# Change Log

All notable s to this project will be documented in this file.

The format is based on [Keep a log](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# v1.3

## v1.3.0 (2022-07-30)
### Feat
- About Page - Adjust rainbow body behavior
- Skill Page - Adjust tag cloud display rules
- Files - Add English CV to public directory
- Theme - Update dark mode theme color

## v1.3.1 (2022-07-30)
### Fixed
- Theme - Fixed dark mode theme color and body color not aligned
- About Page - Fixed bad scroll experience on about page on mobile device (#4)

## v1.3.2 (2022-08-02)
### Added
- Readme - Add Readme document
- Licence - Add MIT Licence
