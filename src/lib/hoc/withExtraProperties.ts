import React from 'react';

function omit<O extends Record<string, unknown>, K extends string>(
  obj: O,
  ...keys: readonly K[]
): Omit<O, K> {
  function process(obj: O, keys: readonly K[]): Omit<O, K> {
    if (keys.length === 0) return obj;
    const { [keys[0]]: _, ...newObj } = obj;
    return newObj;
  }
  return process(obj, keys);
}

export function withExtraProperties<
  C extends React.ComponentType<any>,
  P extends Record<string, unknown>
>(Component: C, properties: P): React.ComponentType<Custom.ComponentProps<C> & Partial<P>> {
  return Object.assign(
    React.memo((props) => React.createElement(Component, omit(props, ...Object.keys(properties)))),
    {
      defaultProps: properties,
    } as unknown
  );
}
