import React from 'react';
import cookie from 'js-cookie';
import { ThemeMode } from 'src/enums/ThemeMode';

const COOKIE_KEY = 'theme-mode';

/** check if the target input string is a valid theme-mode value */
export const isValidThemeMode = (target: string | undefined): target is ThemeMode => {
  if (!target) return false;

  return Object.values<string>(ThemeMode).includes(target);
};

/** get the theme mode from system setting */
export const getSystemThemeMode = () => {
  if (typeof window === 'undefined') return ThemeMode.LIGHT;

  return window.matchMedia('(prefers-color-scheme: dark)').matches
    ? ThemeMode.DARK
    : ThemeMode.LIGHT;
};

/** get the theme mode from previous setting */
export const getCurrentThemeMode = () => {
  const themeMode = cookie.get(COOKIE_KEY);
  if (!isValidThemeMode(themeMode)) return getSystemThemeMode();
  return themeMode;
};

/** set the theme mode to a specific value */
export const setThemeMode = (themeMode: string) => {
  if (!isValidThemeMode(themeMode)) return;
  cookie.set(COOKIE_KEY, themeMode);
};
