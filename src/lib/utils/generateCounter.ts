export function* generateCounter(
  begin: number,
  step: number = 1
): Generator<number, number, number> {
  let n = begin;

  while (true) {
    n += step;
    yield n;
  }
}
