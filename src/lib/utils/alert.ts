/* eslint-disable no-console */

export enum WARNING_TYPE {
  NO_PROVIDER = 'NO_PROVIDER',
}

export const warning = (type: WARNING_TYPE, { name }: { name?: string } = {}) => {
  switch (type) {
    case WARNING_TYPE.NO_PROVIDER:
      return console.warn(
        `The action "${name}" will not going to work if it is not inside the context provider.`
      );

    default:
      console.error(`Unknown wraning type "${type}".`);
  }
};
