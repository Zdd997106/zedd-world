import { useCallback, useEffect, useRef, useState } from 'react';

/**
 * This hook is for the client-support-only value to use.
 *
 * - The first input is a function which return a value that only work on the client side which mean it will lead to the code crash if it's rendering on server-side.
 * - The second input is a value for server side use.
 */
export const useSSR = <F extends (...args: any) => any>(callback: F, valueOnSSR: ReturnType<F>) => {
  const [value, setValue] = useState(valueOnSSR);
  const callbackRef = useRef(callback);
  callbackRef.current = callback;

  const updateValue = useCallback(() => {
    const callback = callbackRef.current;
    setValue(callback());
  }, []);

  useEffect(() => {
    updateValue();
  }, []);

  return [value, updateValue] as const;
};
