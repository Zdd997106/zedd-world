import { isMobile } from 'react-device-detect';
import { useSSR } from 'src/lib/hooks/ssr/useSSR';

/** Detect if the current device is a mobile device */
export const useIsMobile = () => {
  const [state] = useSSR(() => isMobile, false);
  return state;
};
