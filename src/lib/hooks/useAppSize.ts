import { useEffect, useMemo, useRef, useState } from 'react';
import { useHandler } from './useHandler';

const DEFAULT_TARGET_SIZE = { width: 0, height: 0 };

/**
 * telling window height and width when window-size has changed.
 * @description [__Reference__](https://usehooks.com/useWindowSize/)
 * */
export const useConditionalSizeListener: UseConditionalSizeListener = (
  getLabel,
  { target = typeof window === 'undefined' ? undefined : window } = {}
) => {
  const currentSize = useRef<TargetSize>(DEFAULT_TARGET_SIZE);
  const previousSize = useRef<TargetSize>(DEFAULT_TARGET_SIZE);
  const [targetSize, setTargetSize] = useState<TargetSize>(DEFAULT_TARGET_SIZE);
  const unregister = useRef<() => void>(() => null);
  const updateDisconnetRule = (callback: () => void) => {
    unregister.current = callback;
  };
  const getCurrentTargetSize = (): TargetSize => {
    if (target === null) return DEFAULT_TARGET_SIZE;

    const size =
      target === window
        ? {
            width: window.innerWidth,
            height: window.innerHeight,
          }
        : {
            width: (target as HTMLElement).offsetWidth,
            height: (target as HTMLElement).offsetHeight,
          };
    return { ...size, label: getLabel?.(size) };
  };

  const register = useHandler(() => {
    if (!target) return;

    setTargetSize(getCurrentTargetSize());
    const handleResize = () => {
      currentSize.current = getCurrentTargetSize();
      if (getLabel === undefined || currentSize.current?.label !== previousSize.current?.label) {
        previousSize.current = currentSize.current;
        setTargetSize(currentSize.current);
      }
    };
    if (target === window) {
      window.addEventListener('resize', handleResize);
      handleResize();
      updateDisconnetRule(() => window.removeEventListener('resize', handleResize));
      return;
    }

    const resizeListener = new ResizeObserver(handleResize);
    resizeListener.observe(target as HTMLElement);
    updateDisconnetRule(() => {
      resizeListener.disconnect();
    });
  }, [target]);

  useEffect(() => {
    register();
    return () => unregister.current();
  }, []);

  return targetSize;
};
type TargetSize = { height: number; width: number; label?: boolean | string };
type UseConditionalSizeListener = (
  getLabel?: (windowSize: TargetSize) => boolean | string | undefined,
  options?: {
    target?: Window | HTMLElement | null;
  }
) => TargetSize;

/** Detect which range the window size is */
export const useAppSize: UseAppSize = ({ ...gates }, minGateName) => {
  /** rule, to order item by the gate values with asc */
  const orderRule = (a: string, b: string) => (gates?.[b] ?? 0) - (gates?.[a] ?? 0);
  /** get label by window width */
  const getLabel = (width: number): any =>
    Object.keys(gates)
      .sort(orderRule)
      .find((key) => gates[key] <= width);
  // get current window width and label
  const windowSize = useConditionalSizeListener(({ width }) => getLabel(width));

  return useMemo(() => {
    return (windowSize.label as string) ?? minGateName;
  }, [windowSize, minGateName]);
};
type UseAppSize = <K extends { [key: string]: number }, N extends string>(
  gates: K,
  minGateName?: N
) => keyof K | N;

/** Detect which range the element size is */
export const useElementSize: UseElementSize = (target, { ...gates }, minGateName) => {
  /** rule, to order item by the gate values with asc */
  const orderRule = (a: string, b: string) => (gates?.[b] ?? 0) - (gates?.[a] ?? 0);
  /** get label by window width */
  const getLabel = (width: number): any =>
    Object.keys(gates)
      .sort(orderRule)
      .find((key) => gates[key] <= width);
  // get current window width and label
  const windowSize = useConditionalSizeListener(({ width }) => getLabel(width), { target });

  return useMemo(() => {
    return (windowSize.label as string) ?? minGateName;
  }, [windowSize, minGateName]);
};
type UseElementSize = <K extends { [key: string]: number }, N extends string>(
  target: HTMLElement | null,
  gates: K,
  minGateName?: N
) => keyof K | N;
