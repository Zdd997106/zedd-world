import { useCallback, useEffect, useState } from 'react';

/** Return a state-full function, the states in watch list will always be the latest status */
export function useHandler<T extends (...params: any[]) => any>(
  event: T | undefined,
  watch: any[] = []
) {
  /** the value of the idle status of the params */
  const IDLE = null;

  /** the parameters of the handler function */
  const [params, setParams] = useState<unknown[] | null>(IDLE);

  /** a function only changes when one of the item, which in the "watch" list, has changed */
  const callback = useCallback(event ?? (() => null), watch);

  useEffect(
    // when params have changed and params are not on idle status
    () => {
      if (params === IDLE) return;
      // execute callback function with params
      callback(...params);
      // reset params to idle status
      setParams(IDLE);
    },
    [params]
  );

  return useCallback<T>(((...params) => setParams(params)) as T, []);
}
