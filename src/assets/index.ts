import * as Image from './image';
// import * as Svg from './svg'

const Assets = {
  Image,
  // Svg
};

export default Assets;

type RecordKey<R extends Record<string, unknown>> = R extends Record<string, infer T>
  ? T extends Record<string, unknown>
    ? RecordKey<T>
    : keyof R
  : never;

export type AssertID = RecordKey<typeof Assets>;
