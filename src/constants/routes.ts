export const ROOT = '/';

export const RESUME = '/resume';

export const RESUME_ANCHOR = {
  TOP: `${RESUME}#top`,
  PROFILE: `${RESUME}#profile`,
  BACKGROUND: `${RESUME}#background`,
  SKILLS: `${RESUME}#skills`,
  EXPERIENCE: `${RESUME}#experience`,
  SIDE_PROJECT: `${RESUME}#side-project`,
  ACTIVITY: `${RESUME}#activity`,
  AUTOBIOGRAPHY: `${RESUME}#autobiography`,
};

export const ABOUT = '/about';

export const ABOUT_ANCHOR = {
  ABOUT_ME: `${ABOUT}#about-me`,
  ABOUT_WEBSITE: `${ABOUT}#about-website`,
  CONTACT: `${ABOUT}#contact`,
};

export const SKILL = {
  FE: '/skill/frontend',
  BE: '/skill/backend',
};

export const PROJECT = {
  BASE: '/project',
  DROPOUT_STUDENT: '/project/dropout-student',
  ILOD: '/project/ilod',
  TAITRA: '/project/taitra',
  TSMC_ONLINE: '/project/tsmc-online',
  WIFLOW: '/project/wiflow',
  CIRRUS: '/project/cirrus-led',
  SIDE_PROJECT_SHARE: '/project/side-project-share',
  TRAVELINTALE: '/project/travelintale',
  JABOT: '/project/jabot',
  TICTACTOE: '/project/tictactoe',
  MANGA_READY: '/project/manga-ready',
  SPARK_AMPLIFY: '/project/spark-amplify',
};
