export const PACKAGE_AUTHOR = process.env.PACKAGE_AUTHOR as string;
export const [, PACKAGE_AUTHOR_NAME, PACKAGE_AUTHOR_EMAIL] = PACKAGE_AUTHOR.split(/(.+) <(.+)>/);
export const PACKAGE_VERSION = process.env.PACKAGE_VERSION as string;
