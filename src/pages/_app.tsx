import React from 'react';
import { AppProps } from 'next/app';
import Loading from 'nextjs-progressbar';
import AppLayout from 'src/features/AppLayout';

import 'src/styles/globals.css';
import 'react-perfect-scrollbar/dist/css/styles.css';

function MyApp({ Component, router, pageProps }: AppProps) {
  const { route } = router;

  return (
    <AppLayout>
      <Loading color="royalblue" />
      <Component {...pageProps} />
    </AppLayout>
  );
}

export default MyApp;
