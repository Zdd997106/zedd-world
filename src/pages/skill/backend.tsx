import React from 'react';
import { Section as LayoutSection, Section } from 'src/components/layout/Section';
import { MultipleListItem, MultipleParagraph } from 'src/components/text/MultipleDom';
import { SkillBody } from 'src/features/customize/skill';
import { TagCloud } from 'src/features/TagCloud';

export default function BackendSkill({ skills }: Custom.AwaitGetProps<typeof getStaticProps>) {
  return (
    <SkillBody background="linear-gradient(180deg, #b897, transparent 40%)">
      <LayoutSection width="large" justify="center" fullPage>
        <TagCloud data={skills} hue={350} />
      </LayoutSection>
      <Section width="small" justify="top" align="left" fullPage>
        <h1>後端工程技術</h1>
        <MultipleParagraph>
          {[
            '我的後端開發經驗主要是以資料擷取、分析、儲存的方面較有充足的訓練，過去在學士及碩士期間跟隨指導教授進入「資訊擷取實驗室」，而便是以資料面的研究為主要課題。而 API 的方面，則是因為過去開發的上的需求，因此透過自學的方式習得了使用 Node Express 開發 API Server 的技能。',
            '我對於後端的開發最熟悉的工具是 SQL、Node JS 與 Python，個別分別是應用在資料儲存、資料傳遞與資料分析之用途。',
          ]}
        </MultipleParagraph>

        <h2>自我能力評估</h2>
        <MultipleParagraph>
          {[
            '出社會後，我所擔任的職務是前端工程師，這使得我的後端的實務開發經驗相較於前端少了很多，在於畢業之後主要都是透過下班後自己寫 Side Project 的方式來學習。',
            '但在學期間因為實驗室的關係仍是有三年的後端開發經驗，對於各類 Design Pattern 與演算法也都有一定程度的理解，並且加上我也仍持續有透過零碎時間學習，對於後端的技術我也仍是有一定程度的信心',
            '我自己也明白，缺乏開發業界上的後端專案使我對於自己定義的後端結構沒辦法有充分的信心來說「這是一個好的架構」，但是若是能夠有一個前輩願意指導或是有完整定義的後端專案能夠參考，我仍是有自信自己有辦法依靠自身的程式能力，將專案給揣摩到相似的水準上！',
          ]}
        </MultipleParagraph>

        <h2>相關技術</h2>
        <ul>
          <MultipleListItem>{skills.map((x) => x.text)}</MultipleListItem>
        </ul>
      </Section>
    </SkillBody>
  );
}

export async function getStaticProps() {
  const t = {};

  const skills = [
    { text: 'Node Express', value: 9 },
    { text: 'Node.JS', value: 8 },
    { text: 'MSSQL', value: 8 },
    { text: 'Selenium', value: 8 },
    { text: 'Beautiful Soup', value: 8 },
    { text: 'RESTfull API', value: 8 },
    { text: 'Text Mining', value: 7 },
    { text: 'Python', value: 7 },
    { text: 'Socket.IO', value: 5 },
    { text: 'SQLAlchemy', value: 5 },
    { text: 'Machine Learning', value: 5 },
    { text: 'Gitlab CI', value: 5 },
    { text: 'PostgreSQL', value: 3 },
    { text: 'MySQL', value: 3 },
    { text: 'MongoDB', value: 3 },
  ];

  return {
    props: {
      t,
      skills,
    },
  };
}
