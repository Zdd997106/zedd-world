import React from 'react';
import { Section } from 'src/components/layout/Section';
import { MultipleListItem, MultipleParagraph } from 'src/components/text/MultipleDom';
import { SkillBody } from 'src/features/customize/skill';
import { TagCloud } from 'src/features/TagCloud';

export default function FrontendSkill({ skills }: Custom.AwaitGetProps<typeof getStaticProps>) {
  return (
    <SkillBody background="linear-gradient(180deg, #89b7, transparent 40%)">
      <Section width="large" justify="center" fullPage>
        <TagCloud data={skills} hue={200} />
      </Section>
      <Section width="small" justify="top" align="left" fullPage>
        <h1>前端工程技術</h1>
        <MultipleParagraph>
          {[
            '我的前端開發經驗多是以 React 為主，直至 2022 年一共有四年的相關開發經驗，這使我對於 React 這個 Framework 相關的技術都有諸多開發經驗，像是為處理 SSR 而生的 NextJS，或是用來管理表單的 React Hook From、Yup 及 Formik，或用來拆分邏輯的 React Custom Hook 與 React Context 都非常上手。',
          ]}
        </MultipleParagraph>

        <h2>自我能力評估</h2>
        <MultipleParagraph>
          {[
            '我的第一份工作是在 Cloud Interactive 擔任前端工程師，而這使我有更多的機會去觸碰到各種前端技術，以及能夠有機會去與更多不同類型的前端工程師進行交流，因此造就了我目前對於前端會使用到的各項技術相較於後端有著更高的掌握度。',
            '在 CI 的專案開發中，由於我的開發速度較快，經常是被指派去執行 Refactor 的工作，目的在於使專案開發的風格可以統一，並且更好來提供後續的維護。而這樣經歷也使我有更多的時間去思考，究竟要如何把程式碼給寫得有邏輯、有架構，更對於 React 的生態系有了更進一步的理解。',
            '我自己的判斷，我認為現在若面對是以 React 做為開發的 Framework，有自信我有能力可以獨立解決大多的難題！在經歷過數專案的開發與歸納，我有信心自己有能力使用有水準的方法來解決各式難題。',
          ]}
        </MultipleParagraph>

        <h2>相關技術</h2>
        <ul>
          <MultipleListItem>{skills.map((x) => x.text)}</MultipleListItem>
        </ul>
      </Section>
    </SkillBody>
  );
}

export function getStaticProps() {
  const skills = [
    { text: 'Javascript', value: 10 },
    { text: 'React', value: 10 },
    { text: 'TypeScript', value: 8 },
    { text: 'Next.JS', value: 8 },
    { text: 'React Context', value: 8 },
    { text: 'Styled Component', value: 8 },
    { text: 'SASS', value: 8 },
    { text: 'React Hook Form', value: 7 },
    { text: 'Yup', value: 7 },
    { text: 'Material-UI', value: 7 },
    { text: 'React Redux', value: 7 },
    { text: 'Axios', value: 7 },
    { text: 'Webpack', value: 6 },
    { text: 'Formik', value: 5 },
    { text: 'Apollo GraphQL', value: 5 },
    { text: 'Jest', value: 5 },
    { text: 'Redux Saga', value: 4 },
    { text: 'Leaflet', value: 4 },
  ];

  return {
    props: {
      skills,
    },
  };
}
