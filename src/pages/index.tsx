import React from 'react';
import Head from 'next/head';
import Assets from 'src/assets';
import * as Statics from 'src/static-source';
import { CONSTANT_URL, ROUTES, VAR } from 'src/constants';
import { Section as LayoutSection } from 'src/components/layout/Section';
import { MultipleParagraph } from 'src/components/text/MultipleDom';
import { Button } from 'src/components/button/Button';
import { Body as LayoutBody } from 'src/features/AppLayout';
import { SectionImage } from 'src/features/SectionImage';
import { HomePageTitle } from 'src/features/customize/home/styles';

function Home({ t }: Custom.AwaitGetProps<typeof getStaticProps>) {
  return (
    <LayoutBody background="linear-gradient(180deg, #7775, transparent 40%)">
      {/* SEO metas */}
      <Head>
        <title>{`Zedd's World`}</title>
        <meta property="og:image" content={Assets.Image.Home.PROFILE.src} />
      </Head>

      {/* content */}
      <LayoutSection align="center" justify="center" fullPage>
        <HomePageTitle>
          <h1>“{t.title}”</h1>
          <p>—— {VAR.PACKAGE_AUTHOR_NAME}</p>
        </HomePageTitle>

        <SectionImage round imageSize={500} src={Assets.Image.Home.PROFILE} />
      </LayoutSection>

      <LayoutSection justify="top" align="left" width="small">
        <MultipleParagraph>{t.introduction}</MultipleParagraph>
      </LayoutSection>

      <LayoutSection justify="center" align="center" height={200} direction="row">
        <Button type="primary" href={ROUTES.RESUME}>
          {t.gotoResume}
        </Button>

        <Button type="secondary" href={CONSTANT_URL.CV} target="_blank">
          {t.gotoCV}
        </Button>
      </LayoutSection>
    </LayoutBody>
  );
}

export async function getStaticProps() {
  const t = {
    title: Statics.INTRO.motto,
    introduction: Statics.INTRO.description,
    gotoResume: Statics.BUTTON.goto.resume,
    gotoCV: Statics.BUTTON.goto.cv,
  };

  return {
    props: {
      t,
    },
  };
}

export default Home;
