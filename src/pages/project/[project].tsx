import { GetStaticProps } from 'next';
import Head from 'next/head';
import React, { useMemo } from 'react';
import Assets from 'src/assets';
import Statics from 'src/static-source';
import BreadCrumb from 'src/components/common/Breadcrumb';
import { Section as LayoutSection } from 'src/components/layout/Section';
import { Link } from 'src/components/route/CustomLink';
import { MultipleListItem, MultipleParagraph } from 'src/components/text/MultipleDom';
import { ProjectBody } from 'src/features/customize/project';
import { DescriptionObject } from 'src/features/DescriptionObject';
import { SectionImage } from 'src/features/SectionImage';

export default function Project({ t }: Custom.AwaitGetProps<typeof getStaticProps>) {
  const imageSource = useMemo(
    () => Assets.Image.Resume[t.id as keyof typeof Assets.Image.Resume],
    [t.id]
  );
  return (
    <ProjectBody {...t.theme}>
      <Head>
        <title>{`Project - ${t.title}${t.status ? ` (${t.status})` : ''}`}</title>
        <meta property="og:image" content={imageSource.src} />
      </Head>

      <LayoutSection align="left" fullPage>
        <BreadCrumb list={t.breadcrumb} />

        <h1>{t.title}</h1>
        <MultipleParagraph>{t.intro}</MultipleParagraph>

        {t.id in Assets.Image.Resume && (
          <SectionImage src={imageSource} backgroundColor="#fff" maxWidth={700} padding={32} />
        )}
      </LayoutSection>

      <LayoutSection align="left" width="small">
        <h2>使用技術與工具</h2>
        <MultipleParagraph>{t.tools.description}</MultipleParagraph>

        <ul>
          {t.tools.list.map((tool, i) => (
            <React.Fragment key={i}>
              <li>{tool.title}</li>
              <MultipleListItem>
                {tool.items.map(([item, href]) => (
                  <Link key={item} href={href} target="_blank">
                    {item}
                  </Link>
                ))}
              </MultipleListItem>
            </React.Fragment>
          ))}
        </ul>
      </LayoutSection>

      <LayoutSection align="left" width="small" justify="top">
        <h2>專案貢獻</h2>
        <p>
          <DescriptionObject attribute="參與時間" value={t.contribution.time} />
        </p>
        <MultipleParagraph>{t.contribution.content}</MultipleParagraph>
        <p style={{ color: 'var(--page-theme-color)' }}>
          <DescriptionObject attribute="● 專案狀態" value={t.status} />
        </p>
      </LayoutSection>
    </ProjectBody>
  );
}

export async function getStaticPaths() {
  return {
    paths: Object.values(Statics.PROJECT).map((x) => ({ params: { project: x.path } })),
    fallback: false,
  };
}

export async function getStaticProps({ params }: Parameters<GetStaticProps>[0]) {
  const source = Object.values(Statics.PROJECT).find(
    (x) => x.path === params?.project
  ) as typeof Statics.PROJECT.TSMC_ONLINE;

  const t = {
    id: source.id,
    status: source.status,
    theme: source.theme,
    title: source.title,
    breadcrumb: source.breadcrumb,
    intro: source.intro,
    tools: source.tools,
    contribution: source.contribution,
  };

  return {
    props: {
      t,
    },
  };
}
