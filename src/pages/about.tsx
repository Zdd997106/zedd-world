import React from 'react';
import { VAR } from 'src/constants';
import { Link } from 'src/components/route/CustomLink';
import { RainbowBody } from 'src/features/RainbowBody';
import { Section } from 'src/components/layout/Section';

export default function About() {
  return (
    <RainbowBody>
      <Section width="small" justify="top" align="left" fullPage>
        <h2 id="about-me">關於作者</h2>
        <ul>
          <li>
            GitLab：
            <a href="https://gitlab.com/zdd997106" target="_blank" rel="noreferrer">
              gitlab.com/zdd997106
            </a>
          </li>
          <li>
            Linkedin：
            <a href="https://linkedin.com/in/zedd-zhong-339324214" target="_blank" rel="noreferrer">
              linkedin.com/in/zedd-zhong-339324214
            </a>
          </li>
        </ul>

        <p>
          嗨！我是 Zedd
          我是這一個作品及網站的作者，我是一個喜歡追求挑戰的工程師，我喜歡用盡自己的全力去完成事情的感覺，會累，但是這讓我很享受。
          擔任社長、單車環島、徒步環島、單車上武嶺、寫旅遊部落格網站，每一件事真的都不簡單，不過不知道為什麼在做著這些自我挑戰的過程中，總是會得到很多夥伴、很多人的支持，而這些變成為了我不斷前進的動力！
        </p>
        <p>
          而我最近的一個目標就是希望我自己能夠離開台灣出國工作！為了這個目標我必須不斷變強，讓自己在語言與工作能力上都有足夠的實力！
        </p>

        <h2 id="about-website">關於這個網站</h2>
        <ul>
          <li>
            此網站的 Gitlab 連結：
            <a href="https://gitlab.com/zdd997106/zedd-world" target="_blank" rel="noreferrer">
              gitlab.com/zdd997106/zedd-world
            </a>
          </li>
        </ul>
        <p>
          這個網站是屬於 Zedd
          的作品集網站，而這個網站除了作品外也包含了作為履歷載點的功能，有興趣的人可以從{' '}
          <code>
            <Link href="/resume">/resume</Link>
          </code>{' '}
          的位置去查看及下載履歷。
        </p>
        <p>
          這個網站的開發使用的是 Gitflow 的風格來做版本控管，並且有透過 Gitlab-CI
          完成自動化部屬的機制。
        </p>

        <h3>Q: 這個網站花了多少時間完成？</h3>
        <blockquote>
          <h4>A:</h4>
          <p>
            嚴格上來說是花了兩個月，但都是使用下班後零碎的時間來寫的。不過其實主要時間還是花在最後一個月啦~
            前面一個月其實沒有寫很多東西，當時弄完 Gitlab-CI 就停了好幾週沒有動程式碼。
          </p>
          <p>
            有人可能會問這個網站真的有這麼難需要花到這個多時間嗎？我自己是覺得如果在沒有美術設計的狀況下可能差不多就是需要個一個月，因為我自己對於設計方面的能力真的是屬於還需要琢磨的類型
            (汗)，光是設計一 個符合自己喜歡的版面就讓我想得很辛苦
            (覺得平常上班的專案有設計師幫忙先處理好 UI/UX 真的是救星啊
            QAQ)。但是如果真的都不管畫面設計，只是單純把網站生出來的話，以上一版只花了三天就寫完的速度而言，加上要等下班後才能寫的條件下，我自己估計大概會是一週的時間。
          </p>
        </blockquote>

        <h3>Q: 為什麼要製作這個網站？</h3>
        <blockquote>
          <h4>A:</h4>
          <p>
            製作這個網站主要是和找工作相關，想要有一個完整的作品集網站，來提供給資方看以及以此強調自身能力標準為何。
          </p>
          <p>
            而目前這個網站已經是第二版本了，第一個版本大約是在 2021
            年中的時候製作的，但是當時的網站成果我並不是很滿意，主要是當時的程式架構還不夠完整，而且畫面風格我自己也
            覺得應該是可以更好看、看起來更專業一點。而就是這樣的理由，將將！這個網站就這樣出來了~
          </p>
        </blockquote>

        <h3>Q: 網站的後續發展為何？</h3>
        <blockquote>
          <h4>A:</h4>
          <p>
            上一版的網站其實最大的問題就是部屬十分麻煩，有很多零碎的動作需要操作，而現在自動化部屬的機制已經完成，我
            最期待的是希望這個網站能夠在之後達到經常性更新，當我進入新的專案後就可以速速將專案加入到作品集中，以及在
            完成專案後，好好地將我對專案的體悟也都分享在網站上。
          </p>
          <p>
            以目前來說這個網站的風格我是還滿喜歡的，所以應該還不會太早去開發第三代版本，但是應該還是會陸陸續續加上
            一些我覺得有趣的新功能！像是語言切換 (英文版)
            的部分，就是屬於後續已經規劃好會更新的部分了。
          </p>
        </blockquote>

        <h2 id="contact">聯絡資訊</h2>
        <p>
          聯絡信箱：<a href={VAR.PACKAGE_AUTHOR_EMAIL}>{VAR.PACKAGE_AUTHOR_EMAIL}</a>
        </p>
      </Section>
    </RainbowBody>
  );
}
