import React from 'react';
import Head from 'next/head';
import Assets from 'src/assets';
import Statics from 'src/static-source';
import { generateCounter } from 'src/lib/utils/generateCounter';
import { MultipleListItem, MultipleParagraph } from 'src/components/text/MultipleDom';
import { Button } from 'src/components/button/Button';
import { Section as LayoutSection } from 'src/components/layout/Section';
import { DetailHyperLink, ResumeBody, ResumeSectionPart } from 'src/features/customize/resume';
import { SectionImage } from 'src/features/SectionImage';
import { DescriptionObject } from 'src/features/DescriptionObject';
import { TitleWithIndex } from 'src/features/TitleWithIndex';

function Resume({ t }: Custom.AwaitGetProps<typeof getStaticProps>) {
  const counter = generateCounter(1, 1);

  return (
    <ResumeBody background="var(--layout-background)">
      {/* SEO metas */}
      <Head>
        <title>{`Zedd's Resume`}</title>
        <meta property="og:image" content={Assets.Image.Resume.PROFILE.src} />
      </Head>

      {/* contents */}
      <div id="top" />

      <LayoutSection fullPage>
        <h1>{t.slogan.title}</h1>
        <MultipleParagraph>{t.slogan.description}</MultipleParagraph>
      </LayoutSection>

      {/* Profile */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="profile">
          {t.profile.title}
        </TitleWithIndex>
        <SectionImage src={Assets.Image.Resume.PROFILE} round imageSize={500} />
        <MultipleParagraph>{t.profile.description}</MultipleParagraph>
      </LayoutSection>

      {/* Background */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="background">
          {t.background.title}
        </TitleWithIndex>

        {/* Background Descriptions */}
        {t.background.descriptions.map(({ title, items, key }, i) => (
          <React.Fragment key={i}>
            {/* School Image */}
            {isExistsInImageMapping(key) && (
              <SectionImage
                round
                imageSize={400}
                padding="0 40px"
                objectFit="cover"
                src={imageMapping[key]}
              />
            )}
            <div>
              <p>{title}</p>
              <MultipleListItem>{items}</MultipleListItem>
            </div>
          </React.Fragment>
        ))}
      </LayoutSection>

      {/* Skill */}
      <LayoutSection width="small" align="left" justify="top">
        <TitleWithIndex index={counter.next().value} id="skills">
          {t.skill.title}
        </TitleWithIndex>

        {/* Description */}
        <MultipleParagraph>{t.skill.description}</MultipleParagraph>

        <ResumeSectionPart>
          <DetailHyperLink href="/skill/frontend">{t.wording.goto.frontend}</DetailHyperLink>
          <DetailHyperLink href="/skill/backend">{t.wording.goto.backend}</DetailHyperLink>
        </ResumeSectionPart>
      </LayoutSection>

      {/* Project Experience */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="experience">
          {t.project.title}
        </TitleWithIndex>

        {/* Descriptions */}
        <MultipleParagraph>{t.project.description}</MultipleParagraph>

        {/* Project With Preview Information */}
        {t.project.details.map((detail) => (
          <ResumeSectionPart key={detail.key}>
            <h3>{detail.type}</h3>
            {/* School Image */}
            {isExistsInImageMapping(detail.key) && (
              <SectionImage
                src={imageMapping[detail.key]}
                imageSize={750}
                maxHeight={500}
                padding={0}
                margin={0}
                objectFit="contain"
                backgroundColor="var(--styled-background-color)"
                style={{
                  padding: 16,
                }}
              />
            )}
            <h4>
              <DescriptionObject
                attribute={t.wording.description.projectName}
                value={detail.title}
              />
            </h4>
            <MultipleParagraph>{detail.description}</MultipleParagraph>

            {/* Preview Descriptions */}
            <MultipleListItem>
              {['role', 'dates', 'task', 'contribution', 'status'].map((key) => (
                <DescriptionObject
                  key={key}
                  keyName={key}
                  attribute={t.wording.conclusion}
                  value={detail}
                />
              ))}
            </MultipleListItem>

            <DetailHyperLink href={detail.href}>{t.wording.detail}</DetailHyperLink>
          </ResumeSectionPart>
        ))}
      </LayoutSection>

      {/* Side Project */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="side-project">
          {t.sideProject.title}
        </TitleWithIndex>

        {/* Description */}
        <MultipleParagraph>{t.sideProject.description}</MultipleParagraph>

        {/* Project With Preview Description */}
        {t.sideProject.details.map((detail) => (
          <ResumeSectionPart key={detail.key}>
            <h3>{detail.type}</h3>
            {isExistsInImageMapping(detail.key) && (
              <SectionImage
                src={imageMapping[detail.key]}
                imageSize={750}
                maxHeight={600}
                padding={0}
                margin={0}
                objectFit="contain"
                backgroundColor="var(--styled-background-color)"
              />
            )}
            <h4>
              <DescriptionObject attribute={t.wording.description.workName} value={detail.title} />
            </h4>
            <MultipleParagraph>{detail.description}</MultipleParagraph>

            {/* Preview Descriptions */}
            <MultipleListItem>
              {['role', 'dates', 'task', 'status'].map((key) => (
                <DescriptionObject
                  key={key}
                  keyName={key}
                  attribute={t.wording.conclusion}
                  value={detail}
                />
              ))}
            </MultipleListItem>

            {detail.href && (
              <DetailHyperLink href={detail.href}>{t.wording.detail}</DetailHyperLink>
            )}
          </ResumeSectionPart>
        ))}
      </LayoutSection>

      {/* Activity And Achievement */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="activity">
          {t.activity.title}
        </TitleWithIndex>
        <MultipleParagraph>{t.activity.description}</MultipleParagraph>

        {/* Activity and Achievement Preview Description */}
        {t.activity.details.map((detail) => (
          <ResumeSectionPart key={detail.key}>
            <h3>{detail.type}</h3>
            {isExistsInImageMapping(detail.key) && (
              <SectionImage
                src={imageMapping[detail.key]}
                imageSize={750}
                maxHeight={500}
                padding={0}
                margin={0}
                objectFit="cover"
                backgroundColor="var(--styled-background-color)"
                style={{
                  padding: 16,
                }}
              />
            )}
            <h4>{detail.title}</h4>
            <MultipleParagraph>{detail.description}</MultipleParagraph>

            <MultipleListItem>
              {['role', 'dates', 'groupType', 'achievement'].map((key) => (
                <DescriptionObject
                  key={key}
                  keyName={key}
                  attribute={t.wording.conclusion}
                  value={detail}
                />
              ))}
            </MultipleListItem>
          </ResumeSectionPart>
        ))}
      </LayoutSection>

      {/* Autobiography */}
      <LayoutSection width="small" align="left" justify="top" fullPage>
        <TitleWithIndex index={counter.next().value} id="autobiography">
          {t.autobiography.title}
        </TitleWithIndex>

        {/* Content */}
        <MultipleParagraph>{t.autobiography.description}</MultipleParagraph>

        {/* Hobbit and Feature Plan */}
        {t.autobiography.details.map((detail) => (
          <ResumeSectionPart key={detail.key}>
            <h3>{detail.type}</h3>
            {isExistsInImageMapping(detail.key) && (
              <SectionImage
                src={imageMapping[detail.key]}
                imageSize={750}
                maxHeight={320}
                padding={0}
                margin={0}
                objectFit="cover"
                backgroundColor="var(--styled-background-color)"
              />
            )}
            <MultipleParagraph>{detail.description ?? ''}</MultipleParagraph>

            {detail.items?.map((item) => (
              <React.Fragment key={item.title}>
                <h4>{item.title}</h4>
                <MultipleParagraph>{item.description}</MultipleParagraph>
              </React.Fragment>
            ))}
          </ResumeSectionPart>
        ))}
      </LayoutSection>

      <LayoutSection height={200} justify="top" align="center">
        <Button href="/pdf/zedd-cv.pdf" target="_blank" type="primary">
          Download CV
        </Button>
      </LayoutSection>
    </ResumeBody>
  );
}

export async function getStaticProps() {
  const t = {
    slogan: Statics.RESUME.slogan,
    profile: Statics.RESUME.profile,
    background: Statics.RESUME.background,
    skill: Statics.RESUME.skill,
    project: Statics.RESUME.project,
    sideProject: Statics.RESUME.sideProject,
    activity: Statics.RESUME.activity,
    autobiography: Statics.RESUME.autobiography,
    wording: Statics.RESUME.wording,
  };

  return {
    props: {
      t,
    },
  };
}

type ImageSrc = React.ComponentProps<typeof SectionImage>['src'];
const imageMapping = {
  ...Assets.Image.Resume,
  MANGA_READY: Assets.Image.Application.PYTHON_WITH_FRAME,
  WIFLOW: Assets.Image.Resume.WISTRON,
  HOBBITS: Assets.Image.Resume.MEMORY,
  ILOD: Assets.Image.Resume.MOST,
  DSR: Assets.Image.Resume.K12EA,
};

function isExistsInImageMapping(key: string | undefined): key is keyof typeof imageMapping {
  if (!key) return false;
  return Boolean((imageMapping as Record<string, unknown>)[key]);
}

export default Resume;
