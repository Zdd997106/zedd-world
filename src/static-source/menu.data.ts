import * as ROUTES from 'src/constants/routes';
import { ThemeMode } from 'src/enums/ThemeMode';

export enum IDType {
  RESUME = 'RESUME',
  SKILL = 'SKILL',
  NOTES = 'NOTES',
  PROJECTS = 'PROJECTS',
  SIDE_PROJECTS = 'SIDE_PROJECTS',
  SETTING = 'SETTING',
  ABOUT = 'ABOUT',
  THEME = 'THEME',
}

export enum ItemAction {
  SWITCH_THEME = 'SWITCH_THEME',
}

export interface DataItemType {
  title: string;
  href?: string;
  action?: [ItemAction, unknown];
  id?: IDType;
  list?: DataItemType[];
}

export const data: DataItemType[] = [
  {
    id: IDType.RESUME,
    title: 'Resume',
    list: [
      {
        title: 'Top',
        href: ROUTES.RESUME_ANCHOR.TOP,
      },
      {
        title: 'Profile',
        href: ROUTES.RESUME_ANCHOR.PROFILE,
      },
      {
        title: 'Background',
        href: ROUTES.RESUME_ANCHOR.BACKGROUND,
      },
      {
        title: 'Skills',
        href: ROUTES.RESUME_ANCHOR.SKILLS,
      },
      {
        title: 'Experience',
        href: ROUTES.RESUME_ANCHOR.EXPERIENCE,
      },
      {
        title: 'Side Project',
        href: ROUTES.RESUME_ANCHOR.SIDE_PROJECT,
      },
      {
        title: 'Activity Achievement',
        href: ROUTES.RESUME_ANCHOR.ACTIVITY,
      },
      {
        title: 'Autobiography',
        href: ROUTES.RESUME_ANCHOR.AUTOBIOGRAPHY,
      },
    ],
  },
  {
    id: IDType.SKILL,
    title: 'Developing Skill',
    list: [
      {
        title: 'Front End Related',
        href: ROUTES.SKILL.FE,
      },
      {
        title: 'Back End Related',
        href: ROUTES.SKILL.BE,
      },
    ],
  },
  {
    id: IDType.PROJECTS,
    title: 'Projects',
    list: [
      {
        title: 'SparkAmplify',
        href: ROUTES.PROJECT.SPARK_AMPLIFY,
      },
      {
        title: 'TSMC Online',
        href: ROUTES.PROJECT.TSMC_ONLINE,
      },
      {
        title: 'Wiflow',
        href: ROUTES.PROJECT.WIFLOW,
      },
      {
        title: 'Cirrus LED',
        href: ROUTES.PROJECT.CIRRUS,
      },
      {
        title: 'Taitra',
        href: ROUTES.PROJECT.TAITRA,
      },
      {
        title: 'iLOD',
        href: ROUTES.PROJECT.ILOD,
      },
      {
        title: 'DSR System',
        href: ROUTES.PROJECT.DROPOUT_STUDENT,
      },
      {
        id: IDType.SIDE_PROJECTS,
        title: 'Side Projects',
        list: [
          {
            title: 'Side Project Share',
            href: ROUTES.PROJECT.SIDE_PROJECT_SHARE,
          },
          {
            title: 'Travelintale',
            href: ROUTES.PROJECT.TRAVELINTALE,
          },
          // {
          //   title: 'jaBot',
          //   href: ROUTES.PROJECT.JABOT,
          // },
          // {
          //   title: 'Tic Tac Toe',
          //   href: ROUTES.PROJECT.TICTACTOE,
          // },
          // {
          //   title: 'Manga Ready',
          //   href: ROUTES.PROJECT.MANGA_READY,
          // },
        ],
      },
    ],
  },
];

export const about: DataItemType = {
  id: IDType.ABOUT,
  title: 'About',
  list: [
    {
      title: 'About Me',
      href: ROUTES.ABOUT_ANCHOR.ABOUT_ME,
    },
    {
      title: 'About Website',
      href: ROUTES.ABOUT_ANCHOR.ABOUT_WEBSITE,
    },
    {
      title: 'Contact',
      href: ROUTES.ABOUT_ANCHOR.CONTACT,
    },
  ],
};

export const setting: DataItemType = {
  id: IDType.SETTING,
  title: 'Other',
  list: [
    about,
    // {
    //   id: IDType.THEME,
    //   title: 'Switch Theme',
    //   list: [
    //     {
    //       title: 'Light Mode',
    //       action: [ItemAction.SWITCH_THEME, ThemeMode.LIGHT],
    //     },
    //     {
    //       title: 'Dark Mode',
    //       action: [ItemAction.SWITCH_THEME, ThemeMode.DARK],
    //     },
    //   ],
    // },
  ],
};

export const flatData = (() => {
  const result: DataItemType[] = [];
  data.forEach((item) =>
    (function traceDeepFlat(data: DataItemType) {
      if (data.id) {
        if (data.id) result.push(data);
        data.list?.forEach((item) => traceDeepFlat(item));
      }
    })(item)
  );
  return result;
})();
