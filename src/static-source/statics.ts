export * as MENU from './menu.data';
export { default as INTRO } from './intro.json';
export { default as BUTTON } from './button.json';
export { default as RESUME } from './resume.json';
export * as PROJECT from './project';
