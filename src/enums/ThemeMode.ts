export enum ThemeMode {
  DARK = 'DARK',
  LIGHT = 'LIGHT',
}
