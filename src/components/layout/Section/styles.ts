import { withExtraProperties } from 'src/lib/hoc/withExtraProperties';
import styled from 'styled-components';
import * as Types from './types';

const SectionBase = styled.section`
  display: flex;
  justify-content: center;
  width: 100%;

  > div {
    width: var(--section-width);
    max-width: 100%;
    margin: auto;
    display: flex;
    flex-direction: column;
    padding: 16px 16px;
    background-color: inherit;
    position: relative;
  }
`;

const defaultProperties: Types.SectionControlProps = {
  align: 'center',
  justify: 'center',
  width: 'medium',
  fullPage: false,
  height: undefined,
  background: undefined,
  direction: 'column',
};

export const Section = styled(withExtraProperties(SectionBase, defaultProperties))`
  > div {
    align-items: ${({ align }) => {
      switch (align) {
        case 'left':
          return 'flex-start';

        case 'right':
          return 'flex-end';

        default:
          return 'center';
      }
    }};

    width: ${({ width }) => {
      switch (width) {
        case 'large':
          return `1400px`;

        case 'medium':
          return `1000px`;

        case 'small':
          return `750px`;

        default:
          if (typeof width === 'number') return `${width}px`;
          return width;
      }
    }};

    justify-content: ${({ justify }) => {
      switch (justify) {
        case 'top':
          return 'flex-start';

        case 'bottom':
          return 'flex-end';

        case 'center':
        default:
          return 'center';
      }
    }};

    min-height: ${({ fullPage, height }) => {
      if (fullPage) return 'calc(100vh - calc(var(--header-height)))';
      switch (typeof height) {
        case 'number':
          return `${height}px`;

        case 'string':
          return height;

        default:
          return 'auto';
      }
    }};

    flex-direction: ${({ direction }) => direction};

    padding-bottom: ${({ fullPage }) => fullPage && 'calc(var(--header-height))'};
  }
`;
