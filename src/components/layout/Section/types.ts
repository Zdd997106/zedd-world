export type SectionControlProps = {
  align?: 'left' | 'right' | 'center';
  justify?: 'top' | 'center' | 'bottom';
  direction?: 'column' | 'row';
  width?: 'large' | 'medium' | 'small' | '100%' | number;
  height?: number | string;
  background?: string;
  fullPage?: boolean;
};
