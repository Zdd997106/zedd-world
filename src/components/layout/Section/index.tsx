import React from 'react';
import * as Styles from './styles';
import * as Types from './types';

type SectionProps = React.HTMLAttributes<HTMLElement> & Types.SectionControlProps;

export const Section: React.FC<SectionProps> = ({ children, ...props }) => {
  return (
    <Styles.Section {...props}>
      <div>{children}</div>
    </Styles.Section>
  );
};
