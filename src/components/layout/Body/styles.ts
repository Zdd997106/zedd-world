import styled from 'styled-components';

export const Body = styled.main`
  min-height: var(--vh-100);
  margin-top: calc(var(--header-height) * -1);

  > div {
    display: flex;
    flex-direction: column;

    [data-body-label='main'] {
      flex: 1;
    }
  }

  a[href] {
    color: var(--link-color);
  }
`;
