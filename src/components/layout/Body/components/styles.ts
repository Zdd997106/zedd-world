import styled from 'styled-components';

export const HeaderSpace = styled.div`
  height: var(--header-height);
  min-height: var(--header-height);
`;
