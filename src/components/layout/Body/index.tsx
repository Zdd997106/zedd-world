import React from 'react';
import Scrollbar from 'src/components/common/Scrollbar';
import { useLayout } from 'src/components/layout/Layout/hooks';
import { ZIndex } from 'src/enums/ZIndex';
import { HeaderSpace } from './components/HeaderSpace';
import * as Styles from './styles';

interface BodyProps extends React.HTMLAttributes<HTMLElement> {
  background?: string;
}

const options = {
  suppressScrollX: true,
};

export const Body: React.FC<BodyProps> = ({ children, onScroll, background, ...props }) => {
  const {
    state: { isMenuOpened },
  } = useLayout();

  return (
    <Styles.Body {...props} style={{ background, ...props.style }}>
      <Scrollbar
        enableRememberScrollTopOnHash
        options={options}
        visible={!isMenuOpened}
        zIndex={ZIndex.BODY_SCROLLBAR}
        onScroll={onScroll}
      >
        {/* a padding space for header */}
        <HeaderSpace />

        {/* body contents */}
        {children}
      </Scrollbar>
    </Styles.Body>
  );
};
