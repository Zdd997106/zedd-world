import React from 'react';
import * as Styles from './styles';

interface FooterProps extends React.HTMLAttributes<HTMLElement> {}

export const Footer: React.FC<FooterProps> = ({ children, ...props }) => {
  return <Styles.Footer {...props}>{children}</Styles.Footer>;
};
