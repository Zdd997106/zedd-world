import styled from 'styled-components';

export const Footer = styled.footer`
  min-height: 200px;
  background-color: var(--footer-color);
  box-shadow: 0 -1px 0 0 var(--footer-shadow-color);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: ease background-color 0.4s;

  p {
    font-size: small;
    margin: 0.2rem;
  }
`;
