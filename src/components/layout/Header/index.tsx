import React from 'react';
import { ReactComponent as LogoSvg } from 'src/assets/svg/logo.svg';
import { Link } from 'src/components/route/CustomLink';
import * as ROUTES from 'src/constants/routes';
import { useLayout } from 'src/components/layout/Layout/hooks';
import * as Styles from './styles';

interface HeaderProps extends React.HTMLAttributes<HTMLElement> {
  disableFilterOnMenuOpened?: boolean;
  isMenuOpened?: boolean;
  suffix?: React.ReactNode;
}

export const Header: React.FC<HeaderProps> = ({
  children,
  disableFilterOnMenuOpened = true,
  isMenuOpened: propIsMenuOpened,
  suffix,
  ...props
}) => {
  const {
    state: { isMenuOpened, theme },
  } = useLayout();
  return (
    <Styles.Header
      {...props}
      data-filter-flag={disableFilterOnMenuOpened ? !(propIsMenuOpened ?? isMenuOpened) : true}
    >
      {/* a logo on the left of the header */}
      <Styles.HeadLogo key={theme}>
        <Link href={{ pathname: ROUTES.ROOT }}>
          <LogoSvg />
        </Link>
      </Styles.HeadLogo>

      {/* contents */}
      <Styles.HeadBlockMain>{children}</Styles.HeadBlockMain>

      {/* extra contents */}
      <Styles.HeadBlockAside>{suffix}</Styles.HeadBlockAside>
    </Styles.Header>
  );
};
