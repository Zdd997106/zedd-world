import { ZIndex } from 'src/enums/ZIndex';
import styled from 'styled-components';

export const Header = styled.header`
  height: var(--header-height);
  flex: 0 0 var(--header-height);
  display: flex;
  flex-direction: row;
  position: sticky;
  align-items: center;
  top: 0;
  color: var(--header-text-color);
  background-color: var(--header-color);
  transition: ease background-color 0.4s;
  box-shadow: inset 0 -1px 0 0 var(--header-shadow-color);
  backdrop-filter: saturate(180%) blur(12px);
  padding: 0 10px;
  font-size: 0.9em;
  z-index: ${ZIndex.HEADER};

  &[data-filter-flag='false'] {
    background-color: var(--header-color-opacity-0);
  }
`;

export const HeadBlockAside = styled.div`
  flex: 0 0 142px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const HeadBlockMain = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const HeadLogo = styled(HeadBlockAside)`
  svg {
    height: calc(var(--header-height) / 2);
  }
`;
