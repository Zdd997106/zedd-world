import React from 'react';
import Scrollbar from 'src/components/common/Scrollbar';
import { useLayout } from 'src/components/layout/Layout/hooks';
import * as Styles from './styles';

interface MenuSideBlockProps extends React.HTMLAttributes<HTMLElement> {}
export const MenuSideBlock: React.FC<MenuSideBlockProps> = ({ children, ...props }) => {
  const {
    state: { isMenuOpened },
  } = useLayout();
  return (
    <Styles.MenuSideBlock {...props} data-open-flag={isMenuOpened}>
      <Scrollbar>{children}</Scrollbar>
    </Styles.MenuSideBlock>
  );
};
