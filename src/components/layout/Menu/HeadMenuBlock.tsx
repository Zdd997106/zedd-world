import React from 'react';
import { Scrollbar } from 'src/components/common/Scrollbar/styles';
import * as Styles from './styles';

interface HeadMenuBlockProps extends React.HTMLAttributes<HTMLElement> {
  display?: boolean;
}
export const HeadMenuBlock: React.FC<HeadMenuBlockProps> = ({ children, display, ...props }) => {
  return (
    <Styles.HeadMenuBlock data-display-label={Boolean(display)} {...props}>
      <div>
        <Scrollbar options={{ suppressScrollY: true }}>{children}</Scrollbar>
      </div>
    </Styles.HeadMenuBlock>
  );
};
