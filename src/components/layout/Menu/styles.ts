import styled from 'styled-components';

export const MenuBar = styled.ul`
  padding: 0;

  list-style: none;
  display: flex;
  flex-direction: row;
  padding: 0;
  z-index: 11;
  white-space: nowrap;

  li {
    display: flex;
    align-items: center;
    padding: 0 2vw;

    a:hover {
      cursor: pointer;
      text-decoration: underline solid 1px currentColor;
      text-underline-offset: 2px;
    }

    &:hover {
      svg {
        margin-bottom: -4px;
      }
    }

    &[data-open-flag='true'] {
      margin-bottom: -1px;

      svg {
        margin-bottom: 0;
        transform: rotate(180deg);
      }
    }

    svg {
      height: 10px;
      width: 10px;
      margin-left: 5px;
      margin-bottom: -2px;
      transition: margin ease 0.2s;
    }
  }
`;

export const HeadMenuBlock = styled.menu`
  min-height: var(--menu-height);
  display: flex;
  flex-direction: row;
  position: fixed;
  align-items: center;
  justify-content: center;
  top: var(--header-height);
  color: var(--header-text-color);
  background-color: var(--header-color-opacity-0);
  box-shadow: inset 0 -1px 0 0 var(--header-shadow-color);
  width: 100%;
  margin: 0;
  padding-left: 0;
  z-index: 12;
  opacity: 1;
  transition: ease background-color 0.4s, ease opacity 0.2s, ease top 0.2s;

  &[data-display-label='false'] {
    opacity: 0;
    top: calc(var(--header-height) - var(--menu-height));
    transition: ease opacity 0.2s 0.1s, ease top 0.2s;
  }

  > div {
    max-width: 100vw;
    position: relative;
  }
`;

export const MenuButton = styled.button`
  position: absolute;
  right: 10px;
  height: 50px;
  width: 50px;
  border: none;
  border-radius: 50%;
  background-color: transparent;
  transform: rotate(-450deg) scale(0.7);
  transition: all ease 0.4s;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    height: 80%;
    width: 80%;
    fill: currentColor;
  }

  &[data-open-flag='true'] {
    opacity: 1;
    transform: rotate(180deg) scale(1);
  }
`;

export const MenuSideBlock = styled.aside`
  position: fixed;
  top: calc(var(--header-height));
  left: calc(100vw + 20px);
  height: calc(100vh - var(--header-height));
  width: 100vw;
  z-index: 12;
  background-color: var(--header-color);
  transition: left ease 0.4s, opacity ease-in 0.4s;
  box-shadow: -3px 0 6px 0 var(--header-shadow-color);
  opacity: 0;

  &[data-open-flag='true'] {
    left: 0;
    opacity: 1;
    backdrop-filter: saturate(180%) blur(15px);
  }
`;

export const MenuSideBlockItem = styled.div`
  padding-left: 2rem;
  &:last-child {
    min-height: calc(100vh - var(--header-height));
  }

  &:hover p {
    letter-spacing: 0.02rem;
    transform: translateX(-0.1%);
  }

  p {
    font-size: 1.5rem;
    font-weight: bold;
    font-family: var(--font-family-head);
    transition: all ease 0.4s;
  }

  ul {
    padding-left: 2rem;
    list-style: none;

    li {
      font-size: 1rem;
      transition: all ease 0.3s;
      letter-spacing: 0;
      margin-top: 5px;
      white-space: nowrap;

      &:hover {
        letter-spacing: 0.2rem;
        list-style: circle;
        transform: translateX(-2px);
      }

      a {
        display: block;
        margin-right: 4rem;
      }
    }
  }
`;
