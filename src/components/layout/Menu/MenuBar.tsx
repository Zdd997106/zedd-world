import React from 'react';
import * as Styles from './styles';

interface MenuBarProps extends React.HTMLAttributes<HTMLUListElement> {}
export const MenuBar: React.FC<MenuBarProps> = ({ children, ...props }) => {
  return <Styles.MenuBar {...props}>{children}</Styles.MenuBar>;
};
