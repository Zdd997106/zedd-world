import React from 'react';
import { Link } from 'src/components/route/CustomLink';
import { useLayout } from 'src/components/layout/Layout/hooks';
import * as Styles from './styles';

interface MenuSideBlockItemProps extends React.HTMLAttributes<HTMLElement> {
  children?: never;
  list?: { title: string; href: React.ComponentProps<typeof Link>['href'] }[];
}
export const MenuSideBlockItem: React.FC<MenuSideBlockItemProps> = ({ title, list, ...props }) => {
  const {
    action: { toggleMenuOpen },
  } = useLayout();

  return (
    <Styles.MenuSideBlockItem {...props}>
      <p>{title}</p>
      <ul>
        {list?.map(({ title, href }) => (
          <li key={title}>
            <Link href={href} onClick={() => toggleMenuOpen(false)}>
              {title}
            </Link>
          </li>
        ))}
      </ul>
    </Styles.MenuSideBlockItem>
  );
};
