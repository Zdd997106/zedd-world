import React from 'react';
import { ReactComponent as SvgMenuIcon } from 'src/assets/svg/menu-icon.svg';
import { useLayout } from 'src/components/layout/Layout/hooks';
import * as Styles from './styles';

interface MenuButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

export const MenuButton: React.FC<MenuButtonProps> = ({ children, ...props }) => {
  const {
    state: { isMenuOpened },
    action: { toggleMenuOpen },
  } = useLayout();
  return (
    <Styles.MenuButton
      {...props}
      role="presentation"
      data-open-flag={isMenuOpened}
      onClick={() => toggleMenuOpen()}
    >
      <SvgMenuIcon />
    </Styles.MenuButton>
  );
};
