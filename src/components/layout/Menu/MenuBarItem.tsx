import React from 'react';
import { Link } from 'src/components/route/CustomLink';
import { ReactComponent as SvgDownArrow } from 'src/assets/svg/down-arrow.svg';

interface MenuBarItemProps extends React.DetailsHTMLAttributes<HTMLLIElement> {
  menuId?: string;
  opened?: boolean;
  href?: string;
}
export const MenuBarItem: React.FC<MenuBarItemProps> = ({
  children,
  menuId,
  opened,
  href,
  ...props
}) => {
  return (
    <li {...props} data-open-flag={opened}>
      <Link href={href}>
        {children}
        {menuId !== undefined && <SvgDownArrow />}
      </Link>
    </li>
  );
};
