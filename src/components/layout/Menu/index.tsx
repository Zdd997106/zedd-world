import { MenuBar as MenuBarBase } from './MenuBar';
import { MenuBarItem } from './MenuBarItem';
import { MenuSideBlock as MenuSideBlockBase } from './MenuSideBlock';
import { MenuSideBlockItem } from './MenuSideBlockItem';

export const MenuBar = Object.assign(MenuBarBase, {
  Item: MenuBarItem,
});

export const MenuSideBlock = Object.assign(MenuSideBlockBase, {
  Item: MenuSideBlockItem,
});

export { HeadMenuBlock } from './HeadMenuBlock';
