import { useContext } from 'react';
import * as Context from './context';

export const useLayout = () => {
  const state = useContext(Context.stateContext);
  const action = useContext(Context.actionContext);

  return {
    state,
    action,
  };
};
