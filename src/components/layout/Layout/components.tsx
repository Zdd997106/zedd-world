import React, { useEffect, useState } from 'react';
import { isMobile } from 'react-device-detect';
import * as Styles from './styles';

interface WrapperProps extends React.HTMLAttributes<HTMLDivElement> {}

export const Wrapper: React.FC<WrapperProps> = ({ children, ...props }) => {
  const [realHeight, setRealHeight] = useState('100vh');

  useEffect(() => {
    if (isMobile) setRealHeight(`${window.innerHeight}px`);
  }, []);

  return (
    <Styles.Wrapper {...props} data-real-height={realHeight}>
      {children}
    </Styles.Wrapper>
  );
};
