import React, { useCallback, useMemo } from 'react';
import Head from 'next/head';
import { useToggle } from 'react-use';
import { css } from 'styled-components';
import { ThemeMode } from 'src/enums/ThemeMode';
import { Device } from 'src/enums/Device';
import * as Context from './context';
import * as Components from './components';

export interface LayoutProps extends React.ComponentProps<typeof Components.Wrapper> {
  theme?: ThemeMode;
  device?: Device;
}

export const Layout: React.FC<LayoutProps> = ({ children, theme, device, ...props }) => {
  const [isMenuOpened, toggleMenuOpen] = useToggle(Context.defaultState.isMenuOpened);

  // reset all layout states
  const resetStates = useCallback(() => {
    toggleMenuOpen(Context.defaultState.isMenuOpened);
  }, []);

  const state = useMemo(() => {
    return { isMenuOpened, theme: theme ?? ThemeMode.LIGHT, device: device ?? Device.PC };
  }, [isMenuOpened, theme, device]);

  const action = useMemo(() => {
    return {
      resetStates,
      toggleMenuOpen,
    };
  }, [toggleMenuOpen]);

  const value = useMemo(
    () => ({
      state,
      action,
    }),
    [state, action]
  );

  return (
    <Context.Provider value={value}>
      {/* [TODO] A temporary way to fix mobile device dark mode problem */}
      {theme === ThemeMode.DARK && (
        <Head>
          <style>{css`
            body {
              background-color: #222;
            }
          `}</style>
        </Head>
      )}
      <Components.Wrapper {...props} data-device-label={state.device} data-theme={state.theme}>
        {children}
      </Components.Wrapper>
    </Context.Provider>
  );
};
