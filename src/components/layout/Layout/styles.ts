import styled, { css } from 'styled-components';
import { ThemeMode } from 'src/enums/ThemeMode';

const theme = css<{ 'data-real-height'?: string }>`
  // base
  --text-color: #222;
  --vh-100: ${(props) => props['data-real-height']};
  --fixed-height-offset: (100vh - var(--vh-100));
  --shadow-color: #0001;

  // header
  --header-color: #fffa;
  --header-color-opacity-0: #fdfdfd;
  --header-text-color: #000;
  --header-shadow-color: var(--shadow-color);
  --header-height: 56px;

  // body
  --body-color: #fff;
  --body-text-color: var(--text-color);

  // footer
  --footer-color: #fafafa;
  --footer-shadow-color: var(--shadow-color);

  // section
  --section-width: 1000px;

  // menu
  --menu-height: 80px;

  // link
  --link-color: royalblue;
  --link-line-color: #fff;

  &[data-theme='${ThemeMode.DARK}'] {
    // base
    --text-color: #eee;
    --shadow-color: #eee1;

    // header
    --header-color: #1118;
    --header-color-opacity-0: #111;
    --header-text-color: #eee;

    // body
    --body-color: #222;

    // footer
    --footer-color: #111;

    // link
    --link-color: #6787e7;
    --link-line-color: #fff;
  }
`;

export const Wrapper = styled.div`
  ${theme}
  height: var(--vh-100);
  width: 100vw;
  display: flex;
  flex-direction: column;
  position: relative;
  min-width: 300px;
  background-color: var(--body-color);
  transition: ease background-color 0.4s;
  color: var(--body-text-color);

  code {
    padding: 2px 6px;
    border-radius: 5px;
    background-color: #8883;
    display: inline-flex;
    vertical-align: middle;
  }
`;
