import React from 'react';
import { Device } from 'src/enums/Device';
import { warning, WARNING_TYPE } from 'src/lib/utils/alert';
import { ThemeMode } from 'src/enums/ThemeMode';

export interface StateType {
  isMenuOpened: boolean;
  theme: ThemeMode;
  device: Device;
}
export interface ActionType {
  /** reset all state to default */
  resetStates: () => void;
  /** toggle the layout menu to open / close */
  toggleMenuOpen: (open?: boolean) => void;
}

export const defaultState = {
  isMenuOpened: false,
  theme: ThemeMode.LIGHT,
  device: Device.PC,
};

export const defaultAction = {
  resetStates: () => warning(WARNING_TYPE.NO_PROVIDER, { name: 'resetStates' }),
  toggleMenuOpen: () => warning(WARNING_TYPE.NO_PROVIDER, { name: 'toggleMenuOpen' }),
};

export const stateContext = React.createContext<StateType>(defaultState);
export const actionContext = React.createContext<ActionType>(defaultAction);

export const Provider: React.FC<{
  value: { state: StateType; action: ActionType };
}> = ({ value: { state, action }, children }) => {
  return React.createElement(
    stateContext.Provider,
    { value: state },
    React.createElement(actionContext.Provider, { value: action }, children)
  );
};
