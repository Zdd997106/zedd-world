import React from 'react';
import { Link } from 'src/components/route/CustomLink';
import * as Styles from './styles';
import * as Types from './types';

export const Button: React.FC<
  Omit<React.ButtonHTMLAttributes<HTMLButtonElement>, 'type'> & {
    type: 'primary' | 'secondary';
    href?: Types.LinkProps['href'];
    target?: Types.LinkProps['target'];
  }
> = ({ type, href, target, ...props }) => {
  if (href)
    return (
      <Link href={href} target={target}>
        <Styles.Button type="button" data-theme-label={type} {...props} />
      </Link>
    );
  return <Styles.Button type="button" data-theme-label={type} {...props} />;
};
