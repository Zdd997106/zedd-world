import styled, { css } from 'styled-components';

const primaryButtonCss = css`
  color: var(--link-line-color);
  background-color: var(--link-color);
  border: solid 2px var(--link-color);
`;

const secondaryButtonCss = css`
  color: var(--link-color);
  background-color: transparent;
  border: solid 2px var(--link-color);
`;

export const Button = styled.button`
  padding: 8px 24px;
  margin: 16px;
  font-weight: bold;
  cursor: pointer;
  border-radius: 5px;
  transition: all ease 0.1s;

  &[data-theme-label='primary'],
  &[data-theme-label='secondary']:hover {
    ${primaryButtonCss}
  }

  &[data-theme-label='secondary'],
  &[data-theme-label='primary']:hover {
    ${secondaryButtonCss}
  }
`;
