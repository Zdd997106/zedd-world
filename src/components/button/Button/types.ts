import { Link } from 'src/components/route/CustomLink';

export type LinkProps = Custom.Completed<Custom.ComponentProps<typeof Link>>;
