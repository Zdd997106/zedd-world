import styled from 'styled-components';

export const Button = styled.button`
  border: none;
  background-color: transparent;
  padding: 2px;
  cursor: pointer;

  svg {
    color: orange;
  }
`;
