import React from 'react';
import { ReactComponent as SunSvg } from 'src/assets/svg/sun.svg';
import { ReactComponent as MoonSvg } from 'src/assets/svg/moon.svg';
import { ThemeMode } from 'src/enums/ThemeMode';

import * as Styles from './styles';

interface ThemeModeSwitchProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  mode: ThemeMode;
  size?: number;
}
export const ThemeModeSwitch: React.FC<ThemeModeSwitchProps> = ({
  mode,
  size,
  style,
  ...props
}) => {
  return (
    <Styles.Button
      {...props}
      style={{ ...style, height: size ?? style?.height, width: size ?? style?.width }}
    >
      {(() => {
        switch (mode) {
          case ThemeMode.LIGHT:
            return <SunSvg />;
          case ThemeMode.DARK:
            return <MoonSvg />;
          default:
            return null;
        }
      })()}
    </Styles.Button>
  );
};
