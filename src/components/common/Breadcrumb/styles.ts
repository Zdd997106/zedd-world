import styled from 'styled-components';
import { MultipleListItem } from 'src/components/text/MultipleDom';

export const BreadcrumbWrapper = styled(MultipleListItem)`
  display: flex;
  flex-direction: row;
  padding: 0;

  li {
    list-style: none;

    &:not(:first-child)::before {
      margin: 0 6px;
      content: '▸';
    }
  }
`;
