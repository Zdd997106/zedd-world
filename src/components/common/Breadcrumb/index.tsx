import React from 'react';
import { Link } from 'src/components/route/CustomLink';
import * as Styles from './styles';

interface BreadCrumbProps extends React.HTMLAttributes<HTMLUListElement> {
  list: {
    name: string;
    href?: URL | string;
  }[];
}

const BreadCrumb: React.FC<BreadCrumbProps> = ({ list, ...props }) => {
  return (
    <Styles.BreadcrumbWrapper {...props}>
      {list.map(({ name, href }) => (
        <Link href={href} key={name}>
          {name}
        </Link>
      ))}
    </Styles.BreadcrumbWrapper>
  );
};

export default BreadCrumb;
