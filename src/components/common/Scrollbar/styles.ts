import styled, { css } from 'styled-components';
import { forwardRef, createElement } from 'react';
import ReactPerfectScrollbar, { ScrollBarProps } from 'react-perfect-scrollbar';
import { darkModeScope } from 'src/styles/css/darkModeScope';

const defaultProps = {
  visible: true,
  zIndex: 1,
  width: 6,
  gap: 3,
  trackColor: '#0001',
};

interface ScrollbarMiddlewareComponentProps extends ScrollBarProps {
  visible?: boolean;
  zIndex?: number;
  width?: number;
  gap?: number;
  trackColor?: string;
}

const ScrollbarMiddlewareComponent = forwardRef<HTMLDivElement, ScrollbarMiddlewareComponentProps>(
  ({ visible, zIndex, width, gap, trackColor, children, ...props }, ref) =>
    createElement(
      ReactPerfectScrollbar,
      {
        ...props,
        containerRef: ((divElement: HTMLDivElement) => {
          if (!ref) return;
          if (typeof ref === 'function') return ref(divElement);
          // eslint-disable-next-line no-param-reassign
          ref.current = divElement;
        }) as any,
      },
      children
    )
);

ScrollbarMiddlewareComponent.defaultProps = defaultProps;

export const Scrollbar = styled(ScrollbarMiddlewareComponent)`
  --scrollbar-display: ${(props) => (props.visible ?? defaultProps.visible ? 'block' : 'none')};
  --scrollbar-z-index: ${(props) => props.zIndex ?? defaultProps.zIndex};
  --scrollbar-gap: ${(props) => props.gap ?? defaultProps.gap}px;
  --scrollbar-width: ${(props) => props.width ?? defaultProps.width}px;
  --scrollbar-width-with-gaps: var(--scrollbar-width) + var(--scrollbar-gap) * 2;
  --scrollbar-track-color: ${(props) => props.trackColor ?? defaultProps.trackColor};

  > .ps__rail-x,
  > .ps__rail-y {
    display: var(--scrollbar-display);
    z-index: var(--scrollbar-z-index);
  }

  & > .ps__rail-y,
  &:hover > .ps__rail-y {
    width: calc(var(--scrollbar-width-with-gaps) + 0.5px);

    &:hover,
    &.ps--clicking {
      background-color: var(--scrollbar-track-color);
    }

    .ps__thumb-y,
    &:focus > .ps__thumb-y,
    &:hover > .ps__thumb-y,
    &.ps--clicking > .ps__thumb-y {
      border-radius: var(--scrollbar-width);
      width: var(--scrollbar-width);
      right: calc(var(--scrollbar-gap));
    }
  }

  & > .ps__rail-x,
  &:hover > .ps__rail-x {
    height: calc(var(--scrollbar-width-with-gaps) + 0.5px);

    &.ps--active-x:hover,
    &.ps--clicking {
      background-color: var(--scrollbar-track-color);
    }

    .ps__thumb-x,
    &:focus > .ps__thumb-x,
    &:hover > .ps__thumb-x,
    &.ps--clicking > .ps__thumb-x {
      border-radius: var(--scrollbar-width);
      height: var(--scrollbar-width);
      top: calc(var(--scrollbar-gap));
    }
  }

  &:not(.ps--active-x) > .ps__rail-x {
    display: none;
  }
  &:not(.ps--active-y) > .ps__rail-y {
    display: none;
  }

  ${darkModeScope(
    css`
      & {
        .ps__rail-x:hover,
        .ps__rail-y:hover,
        .ps__rail-x:focus,
        .ps__rail-y:focus,
        .ps__rail-x.ps--clicking,
        .ps__rail-y.ps--clicking {
          background-color: transparent;
        }
      }
    `
  )}
`;

export const DeviceDefaultContainer = styled.div`
  position: relative;
  height: 100%;
  overflow: auto;

  &[data-scrollable-x='false'] {
    overflow-x: hidden;
  }
  &[data-scrollable-y='false'] {
    overflow-y: hidden;
  }
`;
