import { useEffect, RefObject } from 'react';
import { useRouter } from 'next/router';

/** hooks for: store scrollTop value on url-hash on routes change */
export function useRememberScrollTopOnHash<E extends HTMLElement>(
  ref: RefObject<E> | false | undefined
) {
  const router = useRouter();
  const refElementReady = Boolean(ref && ref.current);

  useEffect(() => {
    // the effect only work if the ref element is ready
    if (!(ref && ref.current)) return;
    const targetElement = ref.current;

    const handleRouteChange = () => {
      if (!targetElement || !targetElement?.scrollTop) return;

      // remove decimal
      const scrollTop = parseInt(targetElement.scrollTop.toString(), 10);

      // replace hash to the pattern `top=\d{1,}` for anchor on history-back has triggered
      router.replace({ hash: `top=${scrollTop}` });
    };

    // get current hash string
    const { hash } = window.location;

    // to make sure the hash is matched to the anchor pattern
    if (targetElement && /^#top=\d{1,}$/.test(hash)) {
      // anchor the container to the saving position
      targetElement.scrollTop = parseInt(hash.replace('#top=', ''), 10);
    }

    // connect handler to routeChange event
    router.events.on('routeChangeStart', handleRouteChange);

    return () => {
      // disconnect handler from routeChange event
      router.events.off('routeChangeStart', handleRouteChange);
    };
  }, [refElementReady]);
}
