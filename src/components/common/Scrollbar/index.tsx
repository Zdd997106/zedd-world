import React, { useRef } from 'react';
import { useIsMobile } from 'src/lib/hooks/ssr/useIsMobile';
import * as Styles from './styles';
import * as Hooks from './hooks';

export interface ScrollbarProps extends Custom.StyledComponentProps<typeof Styles.Scrollbar> {
  enableRememberScrollTopOnHash?: boolean;
}
const Scrollbar: React.FC<ScrollbarProps> = ({
  children,
  options,
  enableRememberScrollTopOnHash,
  ...props
}) => {
  const isMobile = useIsMobile();
  const ref = useRef<HTMLDivElement>(null);
  Hooks.useRememberScrollTopOnHash(enableRememberScrollTopOnHash && ref);

  return isMobile ? (
    <Styles.DeviceDefaultContainer
      {...props}
      ref={ref}
      data-scrollable-x={!options?.suppressScrollX}
      data-scrollable-y={!options?.suppressScrollY}
    >
      {children}
    </Styles.DeviceDefaultContainer>
  ) : (
    <Styles.Scrollbar
      {...props}
      ref={ref}
      options={{
        scrollXMarginOffset: 1,
        scrollYMarginOffset: 1,
        ...options,
      }}
    >
      {children}
    </Styles.Scrollbar>
  );
};

export default React.memo(Scrollbar);
