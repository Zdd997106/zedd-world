import styled from 'styled-components';

export const StyledImage = styled.span`
  display: inline-flex;
  position: relative;
  max-width: 100%;

  > span {
    background: none !important;
    max-width: inherit !important;
    max-height: inherit !important;
    min-width: inherit !important;
    min-height: inherit !important;
    display: block;
  }

  &[data-object-fit-label='contain'] img {
    height: auto !important;
    width: auto !important;
    min-height: auto !important;
    min-width: auto !important;
  }

  &[data-label-round-flag='true'] > span {
    border-radius: 50%;
    overflow: hidden;
  }
`;
