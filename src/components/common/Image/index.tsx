import React from 'react';
import NextImage from 'next/image';
import * as Styles from './styles';

interface ImageProps extends React.ComponentProps<typeof NextImage> {
  style?: React.CSSProperties;
  maxWidth?: React.CSSProperties['maxWidth'];
  maxHeight?: React.CSSProperties['maxHeight'];
  minWidth?: React.CSSProperties['minWidth'];
  minHeight?: React.CSSProperties['minHeight'];
  round?: boolean;
}

export const Image: React.FC<ImageProps> = ({
  src,
  maxWidth,
  maxHeight,
  minWidth,
  minHeight,
  style,
  round,
  objectFit,
  className,
  ...props
}) => {
  return (
    <Styles.StyledImage
      data-label-round-flag={round}
      data-object-fit-label={objectFit}
      style={{ ...style, maxWidth, maxHeight, minWidth, minHeight }}
      className={className}
    >
      <NextImage loading="eager" src={src} objectFit={objectFit} {...props} />
    </Styles.StyledImage>
  );
};
