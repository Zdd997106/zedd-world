/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import NextLink from 'next/link';

type NextLinkProps = React.ComponentProps<typeof NextLink>;

export type CustomLinkProps = {
  href: NextLinkProps['href'] | undefined | null;
} & Omit<NextLinkProps, 'href'> &
  Omit<React.AnchorHTMLAttributes<HTMLAnchorElement>, keyof NextLinkProps>;
export const CustomLink: React.FC<CustomLinkProps> = ({
  href,
  children,
  as,
  replace,
  shallow,
  passHref,
  prefetch,
  locale,
  ...props
}) => {
  return href ? (
    <NextLink
      href={href}
      as={as}
      replace={replace}
      shallow={shallow}
      passHref={passHref}
      prefetch={prefetch}
      locale={locale}
    >
      <a {...props}>{children}</a>
    </NextLink>
  ) : (
    <a>{children}</a>
  );
};

export const Link = CustomLink;
