import React from 'react';

type DomTag = Parameters<typeof React.createElement>[0];
interface MultipleDomProps {
  tag: DomTag;
  children: string | React.ReactNode[] | React.ReactNode;
}
export const MultipleDom: React.FC<MultipleDomProps & { [key: string]: unknown }> = ({
  tag,
  children,
  ...props
}) => {
  if (Array.isArray(children))
    return <>{children.map((item, i) => React.createElement(tag, { ...props, key: i }, item))}</>;

  if (typeof children !== 'string' || React.isValidElement(children))
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{children ?? null}</>;

  return (
    <>
      {children
        .split(/\n/)
        .filter((x) => x)
        .map((paragraph, i) => React.createElement(tag, { ...props, key: i }, paragraph))}
    </>
  );
};

export const MultipleParagraph: React.FC<
  {
    children: MultipleDomProps['children'];
  } & React.HTMLAttributes<HTMLParagraphElement>
> = ({ children, ...props }) => {
  return (
    <MultipleDom {...props} tag="p">
      {children}
    </MultipleDom>
  );
};

export const MultipleListItem: React.FC<
  {
    children: MultipleDomProps['children'];
  } & React.HTMLAttributes<HTMLUListElement>
> = ({ children, ...props }) => {
  return (
    <ul {...props}>
      <MultipleDom tag="li">{children}</MultipleDom>
    </ul>
  );
};
