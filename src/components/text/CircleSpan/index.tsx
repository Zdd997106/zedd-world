import React from 'react';
import * as Styles from './styles';

const CircleSpan: React.FC<React.HTMLAttributes<HTMLParagraphElement>> = ({
  children,
  ...props
}) => {
  return (
    <Styles.CircleWrapper {...props}>
      <span>{children}</span>
    </Styles.CircleWrapper>
  );
};

export default CircleSpan;
