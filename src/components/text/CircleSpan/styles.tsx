import styled from 'styled-components';

export const CircleWrapper = styled.span`
  display: inline-flex;
  min-height: 1em;
  min-width: 1em;
  height: 1em;
  width: 1em;
  border-radius: 50%;
  color: var(--body-color);
  background-color: var(--body-text-color);
  justify-content: center;
  align-items: center;

  > span {
    position: absolute;
    display: block;
    font-size: 0.65em;
    margin-bottom: 0.05em;
  }
`;
