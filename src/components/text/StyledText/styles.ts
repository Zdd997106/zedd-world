import React from 'react';
import styled, { css } from 'styled-components';

const defaultProps = {
  inline: true,
  angle: 0,
  colorList: [],
  strokeWidth: 0,
  offset: 700,
  position: 0,
  activePosition: 700,
  duration: 2000,
  delay: 0,
  'data-disable': false,
  'data-active': false,
};

interface StyledTextComponentProps extends React.HTMLAttributes<HTMLElement> {
  tag: string;
  inline?: boolean;
  angle?: number;
  colorList?: React.CSSProperties['color'][];
  strokeWidth?: number;
  offset?: number;
  position?: number;
  activePosition?: number;
  duration?: number;
  delay?: number;
  'data-disable'?: boolean;
  'data-active'?: boolean;
}
const StyledTextComponent: React.FC<StyledTextComponentProps> = ({
  children,
  tag,
  inline,
  angle,
  colorList,
  strokeWidth,
  offset,
  position,
  activePosition,
  duration,
  ...props
}) => {
  return React.createElement(tag, props, children);
};
export const StyledText = styled(StyledTextComponent)`
  ${(props) => {
    return css`
      --styled-text-display: ${props.inline ? 'inline-block' : 'block'};
      --styled-text-angle: ${props.angle}deg;
      --styled-text-color-rule: ${props.colorList?.join(',') ?? ''};
      --styled-text-strok-width: ${props.strokeWidth}px;
      --styled-text-offset: ${props.offset}px;
      --styled-text-position: ${props.position}px;
      --styled-text-active-position: ${props.activePosition}px;
      --styled-text-duration: ${props.duration}ms;
      --styled-text-delay: ${props.delay}ms;
    `;
  }}

  --styled-text-left-grow: (0px + var(--styled-text-position));
  --styled-text-right-grow: (var(--styled-text-offset) - var(--styled-text-position));

  display: var(--styled-text-display);
  -webkit-text-stroke: 0.2px #fff;
  padding-right: calc(var(--styled-text-right-grow));
  padding-left: calc(var(--styled-text-left-grow));
  margin-right: calc(var(--styled-text-right-grow) * -1);
  margin-left: calc(var(--styled-text-left-grow) * -1);
  transition: ease all var(--styled-text-duration);

  &[data-active='true'] {
    --styled-text-position: var(--styled-text-active-position);
    transition-delay: var(--styled-text-delay);
  }

  &:not([data-disabled='true']) {
    background: -webkit-linear-gradient(var(--styled-text-angle), var(--styled-text-color-rule));
    -webkit-text-fill-color: transparent;
    -webkit-background-clip: text;
    background-clip: text;
  }
`;
StyledText.defaultProps = defaultProps;
