import React from 'react';
import * as Styles from './styles';

export interface StyledTextProps
  extends Omit<
    Partial<Custom.StyledComponentProps<typeof Styles.StyledText>>,
    'data-disable' | 'data-active'
  > {
  disable?: boolean;
  active?: boolean;
}
export const StyledText: React.FC<StyledTextProps> = ({
  children,
  tag = 'span',
  active,
  disable,
  ...props
}) => {
  const colorList = [
    'currentColor',
    'currentColor',
    ...(props.colorList ?? []),
    'currentColor',
    'currentColor',
  ];
  return (
    <Styles.StyledText
      {...props}
      colorList={colorList}
      tag={tag}
      data-active={Boolean(active)}
      data-disable={Boolean(disable)}
    >
      {children}
    </Styles.StyledText>
  );
};
