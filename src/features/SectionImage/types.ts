import NextImage from 'next/image';

export type StaticImport = Custom.Difference<
  Custom.ComponentProps<typeof NextImage>['src'],
  string
>;
