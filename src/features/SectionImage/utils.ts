import * as Types from './types';

export const getImageRatio = (
  imageSource?: string | Types.StaticImport,
  defaultValue: number = 1
) => {
  if (!imageSource || typeof imageSource === 'string') return defaultValue;

  const target = 'default' in imageSource ? imageSource.default : imageSource;
  return target.height / target.width;
};
