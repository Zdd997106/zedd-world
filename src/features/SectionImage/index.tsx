import React from 'react';
import { Image } from 'src/components/common/Image';
import * as Styles from './styles';
import { getImageRatio } from './utils';

interface SectionImageProps extends React.ComponentProps<typeof Image> {
  imageSize?: number;
  padding?: string | number;
  margin?: string | number;
  autoHeight?: boolean;
  backgroundColor?: string;
}
export const SectionImage: React.FC<SectionImageProps> = ({
  imageSize,
  autoHeight,
  src,
  padding,
  margin,
  ...props
}) => {
  return (
    <div style={{ margin: margin ?? '32px auto' }}>
      <Styles.FrameImage
        src={src}
        width={imageSize}
        height={autoHeight && imageSize ? getImageRatio(src, 1) * imageSize : imageSize}
        maxWidth="100%"
        maxHeight="auto"
        objectFit="cover"
        style={{
          padding,
          ...props.style,
        }}
        {...props}
      />
    </div>
  );
};
