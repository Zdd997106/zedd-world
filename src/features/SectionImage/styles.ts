import styled, { css } from 'styled-components';
import { Image } from 'src/components/common/Image';
import { withExtraProperties } from 'src/lib/hoc/withExtraProperties';

const FrameImageComponent = withExtraProperties(Image, {
  backgroundColor: '#888',
});

export const FrameImage = styled(FrameImageComponent)`
  ${(props) =>
    props.backgroundColor
      ? css`
          background: linear-gradient(0deg, transparent, ${props.backgroundColor}, transparent 95%);
          background-attachment: fixed;
        `
      : ''}
`;
