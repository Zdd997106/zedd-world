import styled from 'styled-components';

export const CloudWrapper = styled.div`
  position: relative;
  height: 750px;
  max-height: 80vh;
  width: 100%;

  > div {
    position: absolute;
    height: 100%;
    width: 100%;

    > div {
      transition: all cubic-bezier(0.37, 0.06, 0.13, 1.01) 1.4s;
    }
  }
`;
