import React, { useEffect, useMemo } from 'react';
import ReactTagCloud from 'react-tag-cloud';
import randomColor from 'randomcolor';
import { useLayout } from 'src/components/layout/Layout/hooks';
import { Device } from 'src/enums/Device';
import * as Styles from './styles';
import * as CONSTANTS from './constants';

export const TagCloud: React.FC<{ hue: number; data: { text: string; value: number }[] }> = ({
  data,
  hue,
}) => {
  const {
    state: { device },
  } = useLayout();

  // pc/mobile mode with different font size
  const isPCDevice = device === Device.PC;

  // the tag cloud will change items position on every re-render triggered, the signal is created to control the timing of it
  const [singal, updateSignal] = React.useState();
  const forceSignalUpdate = React.useCallback(() => updateSignal(Object()), []);

  // trigger signal update by interval
  useEffect(() => {
    if (CONSTANTS.BEGIN_DELAY) setTimeout(forceSignalUpdate, CONSTANTS.BEGIN_DELAY);
    const timer = setInterval(forceSignalUpdate, CONSTANTS.INTERVAL_DELAY);
    return () => clearInterval(timer);
  }, []);

  return useMemo(
    () => (
      <Styles.CloudWrapper>
        <ReactTagCloud
          style={{
            color: () =>
              randomColor({
                hue,
                luminosity: CONSTANTS.LUMINOSITY,
              }),
            padding: CONSTANTS.TEXT_MARGIN,
            fontFamily: CONSTANTS.FONT_FAMILY,
          }}
        >
          {data.map((x) => (
            <div
              key={x.text}
              style={{
                fontSize:
                  (x.value ?? 0) **
                    (CONSTANTS.TEXT_SIZE_BASE + CONSTANTS.TEXT_SIZE_EXPAND_SENSETIVE) *
                  (isPCDevice ? CONSTANTS.PC_TEXT_SIZE_BASE : CONSTANTS.MOBILE_TEXT_SIZE_BASE),
              }}
            >
              {x.text}
            </div>
          ))}
        </ReactTagCloud>
      </Styles.CloudWrapper>
    ),
    [singal, isPCDevice]
  );
};
