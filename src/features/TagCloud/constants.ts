import * as Types from './types';

/** the delay of the first signal update (for letting the user see the effect immediately) */
export const BEGIN_DELAY = 100;

/** the delay of each singal update */
export const INTERVAL_DELAY = 3500;

/** the margin of each cloud tag text */
export const TEXT_MARGIN = 8;

/** the font family of cloud tag text */
export const FONT_FAMILY = 'Passion One';

/** the default base size */
export const TEXT_SIZE_BASE = 1;

/** the base size for pc mode */
export const PC_TEXT_SIZE_BASE = 4;

/** the base size for mobile mode */
export const MOBILE_TEXT_SIZE_BASE = 1.6;

/** the expanding strength of size by value */
export const TEXT_SIZE_EXPAND_SENSETIVE = 0.5;

/** the default luminosity mode */
export const LUMINOSITY: Types.Luminosity = 'bright';
