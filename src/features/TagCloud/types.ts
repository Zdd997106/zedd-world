import randomColor from 'randomcolor';

type RandomColorOptionsMultiple = NonNullable<Parameters<typeof randomColor>[0]>;
type CompletedRandomColorOptionsMultiple = Custom.Completed<RandomColorOptionsMultiple>;

export type Luminosity = CompletedRandomColorOptionsMultiple['luminosity'];
