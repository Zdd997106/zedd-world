import styled, { css } from 'styled-components';
import { darkModeScope } from 'src/styles/css/darkModeScope';
import { Body as LayoutBody } from 'src/features/AppLayout';

export const ResumeBody = styled(LayoutBody)`
  --layout-background: linear-gradient(125deg, #57a5, transparent 40%);
  --styled-background-color: #262e405f 45%, #262e405f 55%;

  ${darkModeScope(
    css`
      --layout-background: linear-gradient(125deg, #cdf5, transparent 40%);
      --styled-background-color: #dde0f08f 30%, #c0d0f08f 50%;
    `
  )}

  h2::before {
    content: '';
    display: block;
    padding-top: var(--header-height);
  }

  h3 {
    &::before {
      content: '◎';
      padding-right: 8px;
      display: inline-block;
    }
  }

  h4 {
    margin-bottom: 0;
  }

  a[href] {
    color: var(--link-color);

    &:hover {
      text-decoration: underline solid 1px;
      text-underline-offset: 2px;
    }
  }
`;

export const ResumeSectionPart = styled.div`
  margin-top: 8px;
  margin-bottom: 24px;
  width: 100%;
`;

export const DetailHyperLink = styled.p`
  color: var(--link-color);
  font-weight: bold;
  font-size: small;
  margin: 0;
  margin-bottom: 1em;
  width: 100%;
  display: flex;
  justify-content: flex-end;

  a::before {
    content: '▸▸▸';
    letter-spacing: -1px;
    padding-right: 4px;
  }
`;
