import React from 'react';
import { Link } from 'src/components/route/CustomLink';
import * as Styles from './styles';

export { ResumeBody } from './styles';
export { ResumeSectionPart } from './styles';

export const DetailHyperLink: React.FC<{ href?: string }> = ({ href, children }) => {
  return (
    <Styles.DetailHyperLink>
      <Link href={href}>{children}</Link>
    </Styles.DetailHyperLink>
  );
};
