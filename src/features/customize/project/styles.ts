import { Body as LayoutBody } from 'src/features/AppLayout';
import { withExtraProperties } from 'src/lib/hoc/withExtraProperties';
import { styledHeadCss } from 'src/styles/css/styledHeadCss';
import styled from 'styled-components';

const ProjectBodyComponent = withExtraProperties(LayoutBody, {
  color: 'currentColor',
  backgroundColor: '#0000',
  colorSpan: 40,
  degree: 0,
});

export const ProjectBody = styled(ProjectBodyComponent)`
  --page-theme-color: ${(props) => props.color};
  --layout-background: linear-gradient(
    ${(props) => props.degree}deg,
    ${(props) => props.backgroundColor},
    transparent ${(props) => props.colorSpan}%
  );
  --link-color: var(--page-theme-color);

  h1 {
    font-size: 2em;
  }

  h2::before {
    content: '';
    display: block;
    padding-top: var(--header-height);
  }

  h1,
  h2 {
    ${styledHeadCss}

    &::after {
      background-color: var(--page-theme-color);
      opacity: 0.2;
    }
  }

  ul > ul {
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 15px;

    li {
      padding-right: 1em;
      min-width: 10em;
    }
  }

  a[href] {
    &:hover {
      text-decoration: underline solid 1px;
      text-underline-offset: 2px;
    }
  }
`;

ProjectBody.defaultProps = {
  ...ProjectBodyComponent.defaultProps,
  background: 'var(--layout-background)',
};
