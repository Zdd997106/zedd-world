import { Body } from 'src/features/AppLayout';
import { styledHeadCss } from 'src/styles/css/styledHeadCss';
import styled from 'styled-components';

export const SkillBody = styled(Body)`
  > div {
    min-height: 50vh;
  }

  h2::before {
    content: '';
    display: block;
    padding-top: var(--header-height);
  }

  h1 {
    font-size: 2em;
  }

  h2 {
    margin-top: 4em;
  }

  h1,
  h2 {
    ${styledHeadCss}

    &::after {
      background-color: #0000;
    }
  }

  ul > ul {
    padding-left: 1em;
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 15px;

    li {
      padding-right: 1em;
      min-width: 10em;
    }
  }
`;
