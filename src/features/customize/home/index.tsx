import React from 'react';
import * as Styles from './styles';

export const HomePageTitle: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({
  children,
  ...props
}) => {
  return <Styles.HomePageTitle {...props}>{children}</Styles.HomePageTitle>;
};
