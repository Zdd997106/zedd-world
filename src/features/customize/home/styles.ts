import styled from 'styled-components';

export const HomePageTitle = styled.div`
  margin-bottom: 32px;
  white-space: pre-line;

  h1 {
    text-align: left;
    padding-left: 0.5em;
    text-indent: -0.5em;
  }
  p {
    text-align: right;
    padding-right: 20px;
  }
`;
