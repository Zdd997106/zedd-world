import React from 'react';

type DescriptionObjectProps =
  | {
      keyName: string;
      delimiter?: string;
      attribute: Record<string, string>;
      value: Record<string, string>;
    }
  | {
      keyName?: never;
      delimiter?: string;
      attribute: string;
      value: string;
    };

export const DescriptionObject: React.FC<DescriptionObjectProps> = ({
  keyName,
  attribute,
  value,
  delimiter = '：',
}) => {
  function getObjectValue(obj: string | number | Record<string, unknown>) {
    if (typeof obj !== 'object' || !keyName) return obj;
    return obj[keyName];
  }
  return <>{[getObjectValue(attribute), getObjectValue(value)].join(delimiter)}</>;
};
