import React from 'react';
import { VAR } from 'src/constants';
import { Footer } from 'src/components/layout/Footer';
import { Body as LayoutBody } from 'src/components/layout/Body';

interface BodyProps extends React.ComponentProps<typeof LayoutBody> {
  background?: string;
}

export const Body: React.FC<BodyProps> = ({ background, children, style, ...props }) => {
  return (
    <LayoutBody {...props} background={background}>
      <div data-body-label="main">{children}</div>

      <Footer>
        <p>版權所有 © 2022 {VAR.PACKAGE_AUTHOR_NAME}</p>
        <p>Copyright © 2022 {VAR.PACKAGE_AUTHOR_NAME} all rights reserved.</p>
        <p>Release: v{VAR.PACKAGE_VERSION}</p>
      </Footer>
    </LayoutBody>
  );
};
