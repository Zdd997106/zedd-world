import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useUpdateEffect } from 'react-use';
import { ThemeModeSwitch } from 'src/components/button/ThemeModeSwitch';
import { useAppSize } from 'src/lib/hooks/useAppSize';
import { getCurrentThemeMode, isValidThemeMode, setThemeMode } from 'src/lib/utils/theme-mode';
import { Device } from 'src/enums/Device';
import { ThemeMode } from 'src/enums/ThemeMode';
import { MENU } from 'src/static-source';
import { Layout } from 'src/components/layout/Layout';
import { Header } from 'src/components/layout/Header';
import { HeadMenuBlock, MenuBar, MenuSideBlock } from 'src/components/layout/Menu';
import { MenuButton } from 'src/components/layout/Menu/MenuButton';
import { Body } from './components/Body';

interface AppLayoutProps extends React.ComponentProps<typeof Layout> {}

const Base: React.FC<AppLayoutProps> = ({ children, ...props }) => {
  const device = useAppSize({ [Device.PC]: 800 }, Device.MOBILE);
  const menuData = MENU.data;
  const [subMenuData, setSubMenuData] = useState<typeof MENU.data>();
  const [rootMenuId, setRootMenuId] = useState<MENU.IDType | null>(null);
  const [theme, setTheme] = useState(ThemeMode.LIGHT);
  const previousSubMenuData = useRef(subMenuData);

  const updateTheme = (theme: string) => {
    if (isValidThemeMode(theme)) {
      setTheme(theme);
      setThemeMode(theme);
    }
  };

  const switchTheme = () => {
    if (theme === ThemeMode.DARK) updateTheme(ThemeMode.LIGHT);
    if (theme === ThemeMode.LIGHT) updateTheme(ThemeMode.DARK);
  };

  const handleClickMenuItem = useCallback(
    (data: MENU.DataItemType) => {
      const newId = rootMenuId === data.id ? null : data.id ?? null;
      setRootMenuId(newId);
      setSubMenuData(newId ? data.list : undefined);
    },
    [rootMenuId]
  );

  const handleClickSubMenuItem = useCallback(
    (data: MENU.DataItemType) => {
      setSubMenuData(data.list);

      if (!data.action) return;
      const [actionType, actionValue] = data.action;
      switch (actionType) {
        case MENU.ItemAction.SWITCH_THEME:
          updateTheme(actionValue as string);
          break;

        default:
          break;
      }
    },
    [rootMenuId]
  );

  // update the theme mode to current theme mode
  useEffect(() => {
    updateTheme(getCurrentThemeMode());
  }, []);

  // update previousSubMenuData on subMenuData change and it is not null
  useUpdateEffect(() => {
    if (!subMenuData) return;
    previousSubMenuData.current = subMenuData;
  }, [subMenuData]);

  // set rootMenuId to null if subMenuData is null
  useUpdateEffect(() => {
    if (subMenuData) return;
    setRootMenuId(null);
  }, [subMenuData]);

  return (
    <Layout {...props} device={device} theme={theme}>
      {device === Device.PC && (
        <>
          <Header
            isMenuOpened={Boolean(rootMenuId)}
            suffix={
              <MenuBar>
                <MenuBar.Item
                  opened={rootMenuId === MENU.setting.id}
                  onClick={() => handleClickMenuItem(MENU.setting)}
                >
                  {MENU.setting.title}
                </MenuBar.Item>
                <ThemeModeSwitch mode={theme} size={30} onClick={switchTheme} />
              </MenuBar>
            }
          >
            <MenuBar>
              {menuData.map((data) => (
                <MenuBar.Item
                  key={data.title}
                  menuId={data.id}
                  opened={rootMenuId === data.id}
                  onClick={() => handleClickMenuItem(data)}
                >
                  {data.title}
                </MenuBar.Item>
              ))}
            </MenuBar>
          </Header>

          <HeadMenuBlock display={subMenuData && subMenuData.length > 0}>
            <MenuBar>
              {(subMenuData ?? previousSubMenuData.current)?.map((data) => (
                <MenuBar.Item
                  key={data.title}
                  menuId={data.id}
                  href={data.href}
                  onClick={() => {
                    handleClickSubMenuItem(data);
                  }}
                >
                  {data.title}
                </MenuBar.Item>
              ))}
            </MenuBar>
          </HeadMenuBlock>
        </>
      )}

      {device === Device.MOBILE && <Header suffix={<MenuButton />} />}

      {device === Device.MOBILE && (
        <MenuSideBlock key={theme}>
          <ThemeModeSwitch
            mode={theme}
            size={40}
            onClick={switchTheme}
            style={{ position: 'absolute', right: 24, top: 24 }}
          />
          {[...MENU.flatData, MENU.about].map((data) => (
            <MenuSideBlock.Item
              key={data.title}
              title={data.title}
              list={data.list
                ?.filter((item) => !item.id)
                .map((item) => ({ title: item.title, href: item.href }))}
            />
          ))}
        </MenuSideBlock>
      )}

      {children}
    </Layout>
  );
};

const AppLayout = Object.assign(Base, {
  Body,
});

export default AppLayout;
export { Body } from './components/Body';
