import React, { useCallback, useRef } from 'react';
import { Body as LayoutBody } from 'src/features/AppLayout';
import { useSSR } from 'src/lib/hooks/ssr/useSSR';
import { useLayout } from 'src/components/layout/Layout/hooks';
import { Device } from 'src/enums/Device';
import { throttleByScrollTop, makeColorfulBackground } from './utils';
import * as CONSTANTS from './constants';

type BodyProps = Custom.ComponentProps<typeof LayoutBody>;
interface RainbowBodyProps extends Omit<BodyProps, 'background'> {}

export const RainbowBody: React.FC<RainbowBodyProps> = ({ children, ...props }) => {
  const {
    state: { device },
  } = useLayout();
  const isMobileDevice = device === Device.MOBILE;

  // use light weight version setting on mobile device
  const colorfulBackgroundLength = isMobileDevice
    ? CONSTANTS.COLORFUL_BACKGROUND_LENGTH_LIGHT
    : CONSTANTS.COLORFUL_BACKGROUND_LENGTH;

  // use light weight version setting on mobile device
  const colorfulBackgroundOptions = isMobileDevice
    ? CONSTANTS.COLORFUL_BACKGROUND_OPTIONS_LIGHT
    : CONSTANTS.COLORFUL_BACKGROUND_OPTIONS;

  // a random color offset as default color offset
  const randomColorRangeOffset = useRef(Math.random() * 360);

  // a adjustment color offset for update dynamicly on scroll-top change
  const adjustmentColorRangeOffset = useRef(0);

  // background state and a function to update it
  const [background, updateBackground] = useSSR(
    () =>
      makeColorfulBackground(colorfulBackgroundLength, {
        ...colorfulBackgroundOptions,
        colorRangeOffset:
          (randomColorRangeOffset.current + adjustmentColorRangeOffset.current) % 360,
      }),
    ''
  );

  // update background on scroll
  const handleScroll = useCallback<React.UIEventHandler<HTMLDivElement>>(
    throttleByScrollTop((e) => {
      const target = e.target as HTMLDivElement;
      adjustmentColorRangeOffset.current = target.scrollTop * CONSTANTS.BACKGROUND_UPDATE_SENSITIVE;
      updateBackground();
    }, CONSTANTS.THROTTLE_UNIT),
    []
  );

  return (
    <LayoutBody {...props} onScroll={handleScroll} background={background}>
      {children}
    </LayoutBody>
  );
};
