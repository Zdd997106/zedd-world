import * as Types from './types';

/** the default length setting for function `makeColorfulBackground` */
export const COLORFUL_BACKGROUND_LENGTH = 14;

/** the default length setting for function `makeColorfulBackground` in a light weight version (which means, it cost lower than normal) */
export const COLORFUL_BACKGROUND_LENGTH_LIGHT = 6;

/** the default options setting for function `makeColorfulBackground` */
export const COLORFUL_BACKGROUND_OPTIONS: Types.ColorfulBackgroundOptions = {
  beginDegree: 85,
  endDegree: 265,
  opacity: 0.1,
  colorRange: 360,
  expandWidth: '20%',
};

export const COLORFUL_BACKGROUND_OPTIONS_LIGHT = {
  ...COLORFUL_BACKGROUND_OPTIONS,
  // in order to keeps the ranbow more clear, increase the value of opacity and expand width
  opacity: 0.2,
  expandWidth: '30%',
  // in order to prevent the end of background cross to the unknown empty space on the bottom of mobile device
  endDegree: 255,
};

/** the change sensitive of background update */
export const BACKGROUND_UPDATE_SENSITIVE = 0.1;

/** the unit pixel of sroll-top change for function throttleByScrollTop */
export const THROTTLE_UNIT = 5;
