import React from 'react';
import * as Types from './types';

export function makeColorfulBackground(
  length: number,
  {
    opacity = 0.2,
    colorRange = 360,
    colorRangeOffset = 0,
    expandWidth = '10%',
    beginDegree = 0,
    endDegree = 360,
  }: Types.ColorfulBackgroundOptions = {}
) {
  const value = (function* nextValue(offset: number, add: number) {
    let value = offset;

    while (true) {
      value += add;
      yield value;
    }
  })(0, (endDegree - beginDegree) / length);

  return new Array(length)
    .fill(null)
    .map(
      (_, i) =>
        `linear-gradient(${beginDegree + value.next().value}deg,  hsla(${
          (colorRangeOffset + (colorRange / length) * i) % 360
        }, 100%, 50%, ${opacity}), transparent ${expandWidth})`
    )
    .join(',');
}

export function throttleByScrollTop<
  H extends React.EventHandler<React.SyntheticEvent<Element, Event>>
>(handler: H, unitPixel: number): H {
  const recordScrollTop = {
    current: 0,
  };

  return ((e) => {
    const target = e.currentTarget;
    const previousScrollTop = recordScrollTop.current;
    const currentScrollTop = target.scrollTop;
    const changedPixel = Math.abs(currentScrollTop - previousScrollTop);

    // do nothing if the pixel changes less than unitPixel config from input
    if (changedPixel < unitPixel) return;

    // update the previous scroll-top value
    recordScrollTop.current = currentScrollTop;
    // run the handler
    handler(e);
  }) as H;
}
