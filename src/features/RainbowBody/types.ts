export interface ColorfulBackgroundOptions {
  opacity?: number;
  colorRange?: number;
  colorRangeOffset?: number;
  expandWidth?: string;
  beginDegree?: number;
  endDegree?: number;
}
