import React from 'react';
import CircleSpan from 'src/components/text/CircleSpan';

interface TitleWithIndexProps extends React.HTMLAttributes<HTMLHeadElement> {
  index: number;
}

export const TitleWithIndex: React.FC<TitleWithIndexProps> = ({ index, children, ...props }) => {
  return (
    <h2 {...props}>
      <CircleSpan>{index}</CircleSpan>
      <span> {children}</span>
    </h2>
  );
};
