declare namespace Custom {
  type React = import('react');

  /** get the prop type from a styled component */
  declare type StyledComponentProps<T> = T extends import('styled-components').StyledComponent<
    infer C,
    any,
    {},
    never
  >
    ? React.ComponentProps<C>
    : never;

  declare type ComponentProps<T> = T extends import('styled-components').StyledComponent<
    any,
    any,
    {},
    never
  >
    ? StyledComponentProps<T>
    : React.ComponentProps<T>;

  /** set un-optional to all of the keys of the object type */
  declare type Completed<T> = { [key in keyof T]-?: T[key] };

  /** get the function type from the type */
  declare type GetFuncionType<T> = T extends Function ? T : never;

  /** get the function type from the type of the children */
  declare type GetChildrenFunctionType<T> = GetFuncionType<React.ComponentProps<T>['children']>;

  /** the intersection of T1 and T2, the key which not in both of the set will be never type */
  declare type Intersection<T1, T2> = {
    [key in keyof (T1 & T2)]: T2 extends Record<key, T1[key]>
      ? T1 extends Record<key, T2[key]>
        ? T1[key] | T2[key]
        : never
      : never;
  };

  /** the exclusive-or of T1 and T2, the key which in both of the set will be never type */
  declare type XOR<T1, T2> = {
    [key in keyof (T1 & T2)]: T2 extends Record<key, T1[key]>
      ? T1 extends Record<key, T2[key]>
        ? never
        : T2[key]
      : T1[key];
  };

  /** only the intersection will keep value type, the exclusive part will be value type with undefine type */
  declare type OneOf<T1, T2> = {
    [key in keyof (T1 & T2)]: T2 extends Record<key, T1[key]>
      ? T1 extends Record<key, T2[key]>
        ? T1[key] | T2[key]
        : T2[key] | undefined
      : T1[key] | undefined;
  };

  declare type AwaitGetProps<GetProps> = Awaited<ReturnType<GetProps>>['props'];

  /**
   * type A exclude the union of type A and type B
   *
   * ```ts
   * type Original = string | number | { key: string }
   *
   * // type Obj is equal to type { key: string }
   * type Obj = Difference<Original, string | number>
   * ```
   */
  declare type Difference<T, Exclude> = T extends Exclude ? never : T;
}
