import { css } from 'styled-components';

export const styledHeadCss = css`
  margin-bottom: 10px;
  position: relative;
  z-index: 2;

  &::after,
  &::before {
    content: '';
    width: 100%;
    display: block;
    height: 20px;
    position: absolute;
    padding-right: 20px;
    padding-top: 5px;
    box-sizing: content-box;
    bottom: -6px;
    left: 15px;
    z-index: -1;
    max-width: 80vw;
  }
  &::before {
    background-color: currentColor;
    opacity: 0.1;
    border-radius: 100% 30% 100% 30%;
    margin-left: -4px;
    margin-top: 8px;
  }
  &::after {
    background-color: currentColor;
    opacity: 0.1;
    border-radius: 30% 100% 30% 100%;
  }
`;
