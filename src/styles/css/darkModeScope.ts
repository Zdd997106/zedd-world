import { ThemeMode } from 'src/enums/ThemeMode';
import { css } from 'styled-components';

export const darkModeScope = <T extends ReturnType<typeof css>>(content: T): T => {
  return css`
    [data-theme=${ThemeMode.DARK}] & {
      ${content}
    }
  ` as T;
};
