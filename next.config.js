const path = require('path');
const { parsed: customEnv } = require('dotenv').config({
  path: path.resolve(__dirname, './.env.local'),
});
const webpack = require('webpack');
const packageRule = require('./package.json');

/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },

  webpack(/**  @type {import('webpack').Configuration} */ original) {
    /**  @type {import('webpack').Configuration} */
    const config = {
      ...original,

      plugins: [
        ...original.plugins,

        // resolve env variables
        new webpack.DefinePlugin({
          'process.env.PACKAGE_VERSION': JSON.stringify(packageRule.version),
          'process.env.PACKAGE_AUTHOR': JSON.stringify(packageRule.author),
        }),
        new webpack.EnvironmentPlugin(customEnv),
      ],

      resolve: {
        ...original.resolve,
        alias: {
          ...original.resolve.alias,

          //  resolve relative path
          src: path.resolve(__dirname, './src'),
        },
      },

      module: {
        ...original.module,
        rules: [
          ...original.module.rules,

          {
            test: /\.svg$/i,
            use: [
              {
                loader: '@svgr/webpack',
                options: {
                  svgoConfig: {
                    removeViewBox: false,
                  },
                },
              },
              {
                loader: 'file-loader',
                options: { name: `assets/icons/[name].[ext]` },
              },
            ],
          },
        ],
      },
    };

    return config;
  },
};
