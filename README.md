# Zedd's World

This is a portfolio website project of Zedd Zhong
 
- Framework: [Next.JS](https://nextjs.org) + [TypeScript](https://www.typescriptlang.org)
- Custom Server: [Node Express](https://expressjs.com)
- Deployment: [Docker](https://www.docker.com) + [GitLab CI](https://docs.gitlab.com/ee/ci)
- Styling: [Styled Component](https://styled-components.com)
- ESlint: [Airbnb](https://github.com/airbnb/javascript)

# Overview

To view the [demo](https://portfolio.zdd997.com).

File Structure

- `src/assets/*` - the static assets including fonts and images.
- `src/components/*` - the common-use react components
- `src/constants/*` - the constant value for the website
- `src/enums/*` - all website common enums variable and type
- `src/features/*` - the special-use react component for the website
- `src/lib/hoc/*` - the common-use high-order-components
- `src/lib/hooks/*` - the common-use custom hooks
- `src/utils/*` - the common-use util function
- `src/pages/*` - the page-related react component
- `src/styled/gloabl.css` - default css rules for the website
- `src/styled/*` - the style related objects
- `src/types/custom.d.ts` - the custom type for the website
- `src/types/module.d.ts` - the module import type declaration
- `src/static-source/*` - constant page data
- `server/*` - the custom server
- `public/*` - assets on public path

Others rule definitions

- Component

    always naming with an upper camel case when it's a file or `index.tsx` in a directory that fitted to the naming rule.

- Materials of a component

    - `*/${Name}/index.tsx` - the main component
    - `*/${Name}/components.tsx` - the related component for the main component
    - `*/${Name}/types.tsx` - the related type definitions
    - `*/${Name}/hooks.ts` - the related custom hook
    - `*/${Name}/styles.ts` - the related styled-component base components
    - `*/${Name}/utils.ts` - the related function
    - `*/${Name}/constants.ts` - the related constant value

    > import directory:
    > 
    > - index
    > - styles > components
    > - components > index
    > - hooks > index
    > - utils > (index / components / hooks / styles)
    > - constants > (index / components / hooks / styles / utils)
    > - types > (index / components / hooks / styles / utils / constants)

# Running Locally

This application requires Node.js v17.8+

First, you need ti create a `.env.local` file similar to `.env.example`.

Then, run the following commands on you commandline/terminal

```cmd
$ git glone https://gitlab.com/Zdd997106/zedd-world.git zedd-world
$ git cd zedd-world
$ npm i
$ npm run dev
```

# Deployment

Set up the following keys to you GitLab CI Variables

- `CONTAINER_NAME` - the docker container name
- `DEPLOY_PORT` - the port to deploy 
- `SSH_PRIVATE_KEY` - the ssh-private-key which it's the pair key (ssh-public-key) registered in you GitLab account
- `SSH_USER` - the target id address to deploy

# Cloning / Forking
Please review the [license](./LICENCE.md) and remove all of my personal information (resume, blog posts, images, etc.).
