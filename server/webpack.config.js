const path = require('path');
const nodeExternals = require('webpack-node-externals');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { parsed: customEnv } = require('dotenv').config({
  path: path.resolve(__dirname, './.env.local'),
});
const webpack = require('webpack');

module.exports = {
  mode: 'production',
  entry: path.join(__dirname, './index.ts'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js',
  },
  target: 'node',
  externalsPresets: { node: true },
  externals: [nodeExternals()],
  resolve: {
    plugins: [new TsconfigPathsPlugin()],
    extensions: ['.js', '.ts'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        include: path.resolve(__dirname, '.'),
        exclude: /\.(test|steps).tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              compilerOptions: {
                noEmit: false,
              },
            },
          },
        ],
      },
    ],
  },
  plugins: [new webpack.EnvironmentPlugin(customEnv)],
};
