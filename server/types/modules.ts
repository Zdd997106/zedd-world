import type * as Socket from 'socket.io';
import type { Server as HTTPServer } from 'http';

declare module 'socket.io' {
  // eslint-disable-next-line no-unused-vars
  export default function (server: HTTPServer): Socket.Server;
  export type SocketServer = Socket.Server;
}
