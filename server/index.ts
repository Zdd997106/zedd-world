import express from 'express';
import next from 'next';
import http from 'http';
import createSocket from './socket';
import createRoutes from './routes';

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

// server config
const HOST = process.env.HOST || '0.0.0.0';
const PORT = parseInt(process.env.PORT || '3000', 10);

nextApp.prepare().then(() => {
  const app = express();
  const server = http.createServer(app);
  const socketServer = createSocket(server);
  const routes = createRoutes({ socketServer });

  // set up body and url parser
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // use custom routes
  app.use(routes);

  // use next-js handler
  app.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(PORT, HOST, () => {
    console.log(`> Ready on http://${HOST === '0.0.0.0' ? 'localhost' : HOST}:${PORT}`);
  });
});
