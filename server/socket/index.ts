import socketIO, { SocketServer } from 'socket.io';
import { v4 as uuid } from 'uuid';
import type { Server as HTTPServer } from 'http';

let queue: string[] = [];

export default (server: HTTPServer) => {
  const io = socketIO(server);

  io.on('connection', (socket) => {
    socket.on('game/matching', (requestRoom) => {
      if (requestRoom && getRoomSize(io, requestRoom) === 0) {
        socket.join(requestRoom);
      } else if (!requestRoom && queue.length === 0) {
        const room = `room-id=${uuid()}`;
        queue.push(room);
        socket.join(room);
        socket.on('disconnect', () => {
          queue = queue.filter((x) => x !== room);
        });
      } else {
        let room;

        if (requestRoom) {
          room = requestRoom;

          if (isRoomFull(io, room)) {
            return socket.emit('game/room-is-full');
          }
        } else {
          room = queue[queue.length - 1];
          queue.pop();
        }

        socket.join(room);
        const player = 'O';

        socket.emit('game/matched', room, player);
        socket.broadcast.to(room).emit('game/matched', room, 'X');
      }
    });

    socket.on('game/join-ttt', (room) => {
      socket.join(room);
      socket.on('game/choose', (place, player) => {
        io.to(room).emit('game/choose', place, player);
      });
      socket.on('disconnect', () => {
        socket.broadcast.to(room).emit('game/player-leave');
      });
    });

    socket.on('game/join-pss', (orignalRoom: string) => {
      let room = orignalRoom;
      room = `pss-room=${room}`;
      socket.join(room);
      socket.on('game/ready', (ready) => {
        socket.broadcast.to(room).emit('game/opponent-ready', ready);
      });
      socket.on('game/show-answer', () => {
        io.to(room).emit('game/show-answer');
      });
      socket.on('game/answer', (answer) => {
        socket.broadcast.to(room).emit('game/opponent-answer', answer);
      });
      socket.on('disconnect', () => {
        socket.broadcast.to(room).emit('game/player-leave');
      });
    });
  });

  return io;
};

const getRoomSize = (io: SocketServer, room: string): number => {
  return (io.sockets.adapter.rooms.get(room) && io.sockets.adapter.rooms.get(room)?.size) ?? 0;
};
const isRoomFull = (io: SocketServer, room: string): boolean => {
  return getRoomSize(io, room) >= 2;
};
