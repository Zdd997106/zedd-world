import { Router } from 'express';
import type { Server as SocketServer } from 'socket.io';

export default ({ socketServer }: { socketServer: SocketServer }) => {
  const router = Router();
  return router;
};
